//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace odm_site.Models
{
    using System;
    
    public partial class FN_DC_GET_SchoolCategory_Result
    {
        public int School_Category_Id { get; set; }
        public string School_Category_Name { get; set; }
    }
}
