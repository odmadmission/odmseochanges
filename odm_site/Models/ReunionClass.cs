﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace odm_site.Models
{
    public class ReunionClass
    {
    }
    public class RegistrationMessagecls
    {
        public string message { get; set; }
        public string otp { get; set; }
        public string mobile { get; set; }
        public int id { get; set; }
    }
    public class OnlineRegistrationcls
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string pname { get; set; }
        public int passoutyear { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string city { get; set; }
        public string classID { get; set; }
       
    }
}