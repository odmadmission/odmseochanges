﻿jQuery(document).ready(function () {

    jQuery("#wp-admin-bar-feed_them_social_admin_bar-default li:first-child a").click(function () {
        //alert('etf');
        console.log('Click Clear Cache Function');

        jQuery.ajax({
            data: { action: "fts_clear_cache_ajax" },
            type: 'POST',
            url: ftsAjax.ajaxurl,
            success: function (response) {
                //	jQuery('body').hide();
                console.log('Well Done and got this from sever: ' + response);
                // alert and upon clicking refresh the page
                if (!alert('Cache for all FTS Feeds cleared!')) { window.location.reload(); }

                return false;
            }
        }); // end of ajax()
        return false;
    }); // end of document.ready
}); // end of form.submit;
(function ($, window, app) {

    $(document).ready(function () {
        var bgClass = "bg-modal";

        function sssInitButtonsClick() {
            $('.supsystic-social-sharing a.social-sharing-button:not(".pinterest")').off('click').on('click', function (e) {
                e.preventDefault();

                if (e.currentTarget.href.slice(-1) !== '#') {
                    window.open(e.currentTarget.href, 'mw' + e.timeStamp, 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                }
            });

            $('.supsystic-social-sharing a.social-sharing-button.pinterest').off('click').on('click', function (e) {
                e.preventDefault();
                var self = this;
                var parentEvent = e;
                var imageLogo;

                e.preventDefault();
                // select image to pin
                if ($('.' + bgClass).length) {
                    $('.' + bgClass).show();
                    return;
                }
                var bgElement = $('<div class="' + bgClass + '"></div>').appendTo(document.body);

                if (theme_data.themeLogo[0] !== 'undefined') {
                    imageLogo = theme_data.themeLogo[0];
                    sssDisplayPageImagesFiltered(bgElement, imageLogo);
                } else {
                    sssDisplayPageImagesFiltered(bgElement);
                }

                $(document).on('click', '.pinterest-image-to-select', function (event) {
                    var src = $(event.target).attr('src');
                    var replaced = $(self).attr('href').replace(/&media=(.*?)&/, '/&media=' + src + '&');
                    $(self).attr('href', replaced);
                    bgElement.hide();
                    if (parentEvent.currentTarget.href.slice(-1) !== '#') {
                        window.open(parentEvent.currentTarget.href, 'mw' + parentEvent.timeStamp, 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                    }
                });
            });
        }
        sssInitButtonsClick();

        $(document).on('mbsMembershipDataLoadEvent', function () {
            sssInitButtonsClick();
        });

        $(document).keyup(function (e) {
            if ($('.' + bgClass).length) {
                if ((e.keyCode === 27) && $('.' + bgClass).is(":visible")) {
                    $('.' + bgClass).hide();
                }
                ;   // esc
            }
        });

        function sssDisplayPageImagesFiltered(bgElement, imageLogo) {

            bgElement.show();
            var images = $('img').filter(function (i) { return $(this).width() > 100 });
            var wrapper = $("<div id='pinterest-images-select-wrapper'></div>").appendTo(bgElement);
            $.each(images, function (i, image) {
                $('<img src="' + $(image).attr('src') + '" class="pinterest-image-to-select">').appendTo(wrapper);
            });
            if (typeof imageLogo !== 'undefined') {
                var filenameBase = imageLogo.substring(0, (imageLogo.length - 12));

                if (bgElement.find('img[src^="' + filenameBase + '"]').length)
                    return;
                $('<img src="' + imageLogo + '" class="pinterest-image-to-select">').appendTo(wrapper);
            }

        }

        window.initSupsysticSocialSharing = function ($container) {
            if (!($container instanceof jQuery)) {
                $container = $($container);
            }

            if (!$container.length) return;

            var $buttons = $container.find('a'),
                animation = $container.attr('data-animation'),
                iconsAnimation = $container.attr('data-icons-animation'),
                buttonChangeSize = $container.attr('data-change-size'),
                $navButton = $container.find('.nav-button'),
                $printButton = $container.find('.print'),
                $bookmarkButton = $container.find('.bookmark'),
                $twitterButton = $container.find('.twitter'),
                $twitterFollowButton = $container.find('.twitter-follow'),
                $mailButton = $container.find('.mail'),
                animationEndEvents = 'webkitAnimationEnd mozAnimationEnd ' +
                    'MSAnimationEnd oanimationend animationend',
                transitionHelper = {
                    'supsystic-social-sharing-right': {
                        'transition': 'translateX(160px)',
                        'display': 'block'
                    },
                    'supsystic-social-sharing-left': {
                        'transition': 'translateX(-160px)',
                        'display': 'block'
                    },
                    'supsystic-social-sharing-top': {
                        'transition': 'translateY(-160px)',
                        'display': 'inline-block'
                    },
                    'supsystic-social-sharing-bottom': {
                        'transition': 'translateY(160px)',
                        'display': 'inline-block'
                    }
                },
                buttonsTransition = null;

            var getAnimationClasses = function (animation) {
                return 'animated ' + animation;
            };

            var checkNavOrientation = function ($c) {
                $.each(transitionHelper, function (index, value) {
                    if (typeof $c.attr('class') !== 'undefined' && ($.inArray(index, $c.attr('class').split(' ')) > -1)) {
                        $c.find('.nav-button').css({
                            'display': value['display']
                        });

                        buttonsTransition = value['transition'];
                    }
                });
            };

            var initNetworksPopup = function () {
                var $networksContainer = $('.networks-list-container'),
                    $button = $('.list-button');

                $button.on('click', function () {
                    $networksContainer.removeClass('hidden')
                        .bPopup({
                            position: [0, 200]
                        });
                });
            };


            if ($buttons.length) {
                $buttons.hover(function () {
                    $(this).addClass(getAnimationClasses(animation))
                        .one(animationEndEvents, function () {
                            $(this).removeClass(getAnimationClasses(animation));
                        });
                    $(this).find('i.fa').addClass(getAnimationClasses(iconsAnimation))
                        .one(animationEndEvents, function () {
                            $(this).removeClass(getAnimationClasses(iconsAnimation));
                        });
                });
                var pinterestBtn = $buttons.filter('.pinterest');
                if (pinterestBtn && pinterestBtn.size()) {
                    var $img = sssFindMostImportantImg();
                    if ($img) {
                        // fix for gallery integration, when using top and bottom together
                        var newHref = pinterestBtn.attr('href');
                        if (!newHref || (newHref && newHref.indexOf && newHref.indexOf('media') == -1)) {
                            var imgUrl = $img.attr('src');
                            // check if LazyLoad image exists
                            if ($img.attr('data-gg-real-image-href')) {
                                imgUrl = $img.attr('data-gg-real-image-href');
                            }
                            newHref = newHref + '&media=' + encodeURIComponent(imgUrl);
                        }
                        if (!newHref || (newHref && newHref.indexOf && newHref.indexOf('description') == -1)) {
                            newHref = newHref + '&description=' + encodeURIComponent(pinterestBtn.attr('data-description'));
                        }

                        pinterestBtn.attr('href', newHref);
                    }
                }
            }

            checkNavOrientation($container);
            $navButton.on('click', function () {
                if ($(this).hasClass('hide')) {
                    $(this).removeClass('hide').addClass('show');

                    $container
                        .find('a').css('transform', buttonsTransition);

                    $container
                        .find('.list-button').css('transform', buttonsTransition);
                } else {
                    $(this).addClass('hide').removeClass('show');

                    $container.find('a').css('transform', 'translateX(0)');

                    $container
                        .find('.list-button').css('transform', 'translateX(0)');
                }
            });

            initNetworksPopup();

            $printButton.on('click', function () {
                window.print();
            });

            $bookmarkButton.on('click', function () {
                if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
                    window.sidebar.addPanel(document.title, window.location.href, '');
                } else if (window.external && ('AddFavorite' in window.external)) { // IE Favorite
                    window.external.AddFavorite(location.href, document.title);
                } else if (window.opera && window.print) { // Opera Hotlist
                    this.title = document.title;
                    return true;
                } else { // webkit - safari/chrome
                    alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
                }
            });

            if ($twitterButton.length) {
                $twitterButton.each(function () {
                    var name = $(this).data('name');
                    if (name.length) {
                        if (name.indexOf("@") < 0) {
                            name = '@' + name;
                        }
                        let href = $(this).attr('href');
                        $(this).attr('href', href + ' via ' + name);
                    }
                });
            }

            if ($twitterFollowButton.length) {
                loadTwitterWidgetApi();

                $twitterFollowButton.each(function () {
                    var name = $(this).data('name');

                    $(this)
                        .attr('href', 'https://twitter.com/intent/follow?screen_name=' + name);
                });
            }

            if ($container.find('a').hasClass('have-all-counter') && $('.counter').length) {
                var summ = 0;

                $('.counter').each(function () {
                    var counter = parseInt($(this).text());

                    if (typeof counter === 'number') summ += counter;
                });

                var htmlTotalCounter = '<div class="supsystic-social-sharing-total-counter counter-wrap">';
                htmlTotalCounter += '<span>Shares</span> ';
                htmlTotalCounter += '<span>' + summ + '</span>';
                htmlTotalCounter += '</div>';
                $container.prepend(htmlTotalCounter);
            }

            if ($container.data('text')) {
                var text = $container.data('text');
                if (!$container.find('.ppsTextAfter').length > 0) {
                    var htmlButtons = '<div class="ppsTextAfter">';
                    htmlButtons += '<span>' + text + '</span>';
                    htmlButtons += '</div>';
                    $container.append(htmlButtons);
                }
            }


            $mailButton.each(function () {
                var url = encodeURIComponent(window.location.href);
                var mailTo = (jQuery(this).attr('data-mailto').length > 0) ? jQuery(this).attr('data-mailto') : '';
                if ($(this).parent().hasClass('supsystic-social-homepage')) {
                    url += '?p=' + $(this).attr('data-post-id');
                }

                var src = 'mailto:' + mailTo + '?subject=' + encodeURIComponent(document.title) + '&body=' + url;
                $(this).attr('href', src);
            });

            $('div.supsystic-social-sharing-bottom a.social-sharing-button.tooltip-icon').tooltipster({
                animation: 'swing',
                position: 'top',
                theme: 'tooltipster-shadow'
            });

            $('div.supsystic-social-sharing-top a.social-sharing-button.tooltip-icon, div.supsystic-social-sharing-content a.social-sharing-button.tooltip-icon').tooltipster({
                animation: 'swing',
                position: 'bottom',
                theme: 'tooltipster-shadow'
            });

            $('div.supsystic-social-sharing-left a.social-sharing-button.tooltip-icon').tooltipster({
                animation: 'swing',
                position: 'right',
                theme: 'tooltipster-shadow'
            });

            $('div.supsystic-social-sharing-right a.social-sharing-button.tooltip-icon').tooltipster({
                animation: 'swing',
                position: 'left',
                theme: 'tooltipster-shadow'
            });

            $container.addClass('supsystic-social-sharing-init');

            var containerShow = false;

            if ($container.hasClass('supsystic-social-sharing-hide-on-mobile')) {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    containerShow = false;
                } else {
                    if (!$container.hasClass('supsystic-social-sharing-click')) {
                        containerShow = true;
                    }
                }
            } else if ($container.hasClass('supsystic-social-sharing-show-only-on-mobile')) {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    if (!$container.hasClass('supsystic-social-sharing-click')) {
                        containerShow = true;
                    }
                } else {
                    containerShow = false;
                }
            } else if (!$container.hasClass('supsystic-social-sharing-click')) {
                $container.addClass('supsystic-social-sharing-loaded');
                containerShow = true;
            }

            if ($container.hasClass('supsystic-social-sharing-hide-on-homepage')
            && $('body').hasClass('home')) {
                containerShow = false;
            }

            if (containerShow)
                $container.show();
            else
                $container.hide();
        };

        var onResize = function () {
            $('.supsystic-social-sharing-left, .supsystic-social-sharing-right').each(function (index, container) {
                var $container = $(container),
                    outerheight = $container.outerHeight(true),
                    totalHeighht = $(window).height();

                $container.animate({ top: totalHeighht / 2 - outerheight / 2 }, 200);
            });
        };

        onResize.call();
        $(window).on('resize', onResize);

        $(document).on('click', function () {
            var $projectContainer = $('.supsystic-social-sharing-click');

            if ($projectContainer.hasClass('supsystic-social-sharing-hide-on-homepage')
            && $projectContainer.hasClass('supsystic-social-homepage'))
                return;

            $projectContainer.show();
        });

        // Init social sharing.
        $('.supsystic-social-sharing:not(.supsystic-social-sharing-init)').each(function (index, el) {
            window.initSupsysticSocialSharing(el);
        });

        document.body.addEventListener("DOMSubtreeModified", function () {
            $('.supsystic-social-sharing:not(.supsystic-social-sharing-init)').each(function (index, el) {
                window.initSupsysticSocialSharing(el);
            });
        }, false);
    });

}(window.jQuery, window));
function sssFindMostImportantImg() {
    var $img = null;
    var findWhere = ['.woocommerce-main-image', 'article', '.entry-content', 'body'];
    for (var i = 0; i < findWhere.length; i++) {
        $img = _sssFindImg(jQuery(findWhere[i]));
        if ($img)
            break;
    }
    return $img;
}
function _sssFindImg($el) {
    if ($el && $el.size()) {
        var $img = null;
        $el.each(function () {
            $img = jQuery(this).find('img');
            if ($img && $img.size()) {
                return false;
            }
        });
        return $img && $img.size() ? $img : false;
    }
    return false;
}
function loadTwitterWidgetApi() {
    window.twttr = (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function (f) {
            t._e.push(f);
        };

        return t;
    }(document, "script", "twitter-wjs"));
};

(function (c) { c.fn.bPopup = function (A, E) { function L() { a.contentContainer = c(a.contentContainer || b); switch (a.content) { case "iframe": var d = c('<iframe class="b-iframe" ' + a.iframeAttr + "></iframe>"); d.appendTo(a.contentContainer); t = b.outerHeight(!0); u = b.outerWidth(!0); B(); d.attr("src", a.loadUrl); l(a.loadCallback); break; case "image": B(); c("<img />").load(function () { l(a.loadCallback); F(c(this)) }).attr("src", a.loadUrl).hide().appendTo(a.contentContainer); break; default: B(), c('<div class="b-ajax-wrapper"></div>').load(a.loadUrl, a.loadData, function (d, b, e) { l(a.loadCallback, b); F(c(this)) }).hide().appendTo(a.contentContainer) } } function B() { a.modal && c('<div class="b-modal ' + e + '"></div>').css({ backgroundColor: a.modalColor, position: "fixed", top: 0, right: 0, bottom: 0, left: 0, opacity: 0, zIndex: a.zIndex + v }).appendTo(a.appendTo).fadeTo(a.speed, a.opacity); C(); b.data("bPopup", a).data("id", e).css({ left: "slideIn" == a.transition || "slideBack" == a.transition ? "slideBack" == a.transition ? f.scrollLeft() + w : -1 * (x + u) : m(!(!a.follow[0] && n || g)), position: a.positionStyle || "absolute", top: "slideDown" == a.transition || "slideUp" == a.transition ? "slideUp" == a.transition ? f.scrollTop() + y : z + -1 * t : p(!(!a.follow[1] && q || g)), "z-index": a.zIndex + v + 1 }).each(function () { a.appending && c(this).appendTo(a.appendTo) }); G(!0) } function r() { a.modal && c(".b-modal." + b.data("id")).fadeTo(a.speed, 0, function () { c(this).remove() }); a.scrollBar || c("html").css("overflow", "auto"); c(".b-modal." + e).unbind("click"); f.unbind("keydown." + e); k.unbind("." + e).data("bPopup", 0 < k.data("bPopup") - 1 ? k.data("bPopup") - 1 : null); b.undelegate(".bClose, ." + a.closeClass, "click." + e, r).data("bPopup", null); clearTimeout(H); G(); return !1 } function I(d) { y = k.height(); w = k.width(); h = D(); if (h.x || h.y) clearTimeout(J), J = setTimeout(function () { C(); d = d || a.followSpeed; var e = {}; h.x && (e.left = a.follow[0] ? m(!0) : "auto"); h.y && (e.top = a.follow[1] ? p(!0) : "auto"); b.dequeue().each(function () { g ? c(this).css({ left: x, top: z }) : c(this).animate(e, d, a.followEasing) }) }, 50) } function F(d) { var c = d.width(), e = d.height(), f = {}; a.contentContainer.css({ height: e, width: c }); e >= b.height() && (f.height = b.height()); c >= b.width() && (f.width = b.width()); t = b.outerHeight(!0); u = b.outerWidth(!0); C(); a.contentContainer.css({ height: "auto", width: "auto" }); f.left = m(!(!a.follow[0] && n || g)); f.top = p(!(!a.follow[1] && q || g)); b.animate(f, 250, function () { d.show(); h = D() }) } function M() { k.data("bPopup", v); b.delegate(".bClose, ." + a.closeClass, "click." + e, r); a.modalClose && c(".b-modal." + e).css("cursor", "pointer").bind("click", r); N || !a.follow[0] && !a.follow[1] || k.bind("scroll." + e, function () { if (h.x || h.y) { var d = {}; h.x && (d.left = a.follow[0] ? m(!g) : "auto"); h.y && (d.top = a.follow[1] ? p(!g) : "auto"); b.dequeue().animate(d, a.followSpeed, a.followEasing) } }).bind("resize." + e, function () { I() }); a.escClose && f.bind("keydown." + e, function (a) { 27 == a.which && r() }) } function G(d) { function c(e) { b.css({ display: "block", opacity: 1 }).animate(e, a.speed, a.easing, function () { K(d) }) } switch (d ? a.transition : a.transitionClose || a.transition) { case "slideIn": c({ left: d ? m(!(!a.follow[0] && n || g)) : f.scrollLeft() - (u || b.outerWidth(!0)) - 200 }); break; case "slideBack": c({ left: d ? m(!(!a.follow[0] && n || g)) : f.scrollLeft() + w + 200 }); break; case "slideDown": c({ top: d ? p(!(!a.follow[1] && q || g)) : f.scrollTop() - (t || b.outerHeight(!0)) - 200 }); break; case "slideUp": c({ top: d ? p(!(!a.follow[1] && q || g)) : f.scrollTop() + y + 200 }); break; default: b.stop().fadeTo(a.speed, d ? 1 : 0, function () { K(d) }) } } function K(d) { d ? (M(), l(E), a.autoClose && (H = setTimeout(r, a.autoClose))) : (b.hide(), l(a.onClose), a.loadUrl && (a.contentContainer.empty(), b.css({ height: "auto", width: "auto" }))) } function m(a) { return a ? x + f.scrollLeft() : x } function p(a) { return a ? z + f.scrollTop() : z } function l(a, e) { c.isFunction(a) && a.call(b, e) } function C() { z = q ? a.position[1] : Math.max(0, (y - b.outerHeight(!0)) / 2 - a.amsl); x = n ? a.position[0] : (w - b.outerWidth(!0)) / 2; h = D() } function D() { return { x: w > b.outerWidth(!0), y: y > b.outerHeight(!0) } } c.isFunction(A) && (E = A, A = null); var a = c.extend({}, c.fn.bPopup.defaults, A); a.scrollBar || c("html").css("overflow", "hidden"); var b = this, f = c(document), k = c(window), y = k.height(), w = k.width(), N = /OS 6(_\d)+/i.test(navigator.userAgent), v = 0, e, h, q, n, g, z, x, t, u, J, H; b.close = function () { r() }; b.reposition = function (a) { I(a) }; return b.each(function () { c(this).data("bPopup") || (l(a.onOpen), v = (k.data("bPopup") || 0) + 1, e = "__b-popup" + v + "__", q = "auto" !== a.position[1], n = "auto" !== a.position[0], g = "fixed" === a.positionStyle, t = b.outerHeight(!0), u = b.outerWidth(!0), a.loadUrl ? L() : B()) }) }; c.fn.bPopup.defaults = { amsl: 50, appending: !0, appendTo: "body", autoClose: !1, closeClass: "b-close", content: "ajax", contentContainer: !1, easing: "swing", escClose: !0, follow: [!0, !0], followEasing: "swing", followSpeed: 500, iframeAttr: 'scrolling="no" frameborder="0"', loadCallback: !1, loadData: !1, loadUrl: !1, modal: !0, modalClose: !0, modalColor: "#000", onClose: !1, onOpen: !1, opacity: .7, position: ["auto", "auto"], positionStyle: "absolute", scrollBar: !0, speed: 250, transition: "fadeIn", transitionClose: !1, zIndex: 9997 } })(jQuery);
(function ($) {
    $(document).ready(function () {
        function ssClickHandler(e, clickedBy) {
            var $button = this != document ? $(this) : $(clickedBy),
                projectId = parseInt($button.data('pid')),
                networkId = parseInt($button.data('nid')),
                postId = parseInt($button.data('post-id')),
				additionalObjectCode = $button.attr('data-plugin-code'),
                data = {},
                url = $button.data('url');

            if ($button.hasClass('trigger-popup')) {
                return;
            }

            data.action = 'social-sharing-share';
            data.project_id = projectId;
            data.network_id = networkId;
            data.post_id = isNaN(postId) ? null : postId;
            if (additionalObjectCode == 'mbs') {
                var additionalObjectItemId = parseInt($button.attr('data-plugin-item-id'))
				, additionalObjectItemType = $button.attr('data-plugin-item-code');
                if (!isNaN(additionalObjectItemId)) {
                    data.additional_object_code = additionalObjectCode;
                    data.additional_object_item_id = additionalObjectItemId;
                    data.additional_object_item_type = additionalObjectItemType;
                }
            }

            $.post(url, data).done(function () {
                $button.find('.counter').text(function (index, text) {
                    if (isNaN(text)) {
                        return text;
                    }

                    return parseInt(text) + 1;
                });
            });

            /** e.preventDefault(); **/
        };

        $(document.body).on('click', '.supsystic-social-sharing a.social-sharing-button', ssClickHandler);
        $(document).on('ssSocialClick', ssClickHandler);
    });
}(jQuery));

jQuery(function () { jQuery(".fts-twitter-div, .fts-jal-fb-group-display, .fts-instagram, .fts-yt-videogroup, .fts-pinterest-boards-wrap").append("<a class='fts-powered-by-text' href='https://www.slickremix.com' target='_blank'>Powered by Feed Them Social</a>"); jQuery("body").addClass('fts-powered-by-text-popup'); });
function slickremixImageResizing() { var e = jQuery(".fts-instagram-inline-block-centered"), t = jQuery(".slicker-instagram-placeholder"), s = e.attr("data-ftsi-columns"), a = e.attr("data-ftsi-margin"), i = 2 * parseFloat(a), r = e.width(), o = e.attr("data-ftsi-force-columns"); if ("1" === s || "2" === s || "3" === s || "4" === s || "5" === s || "6" === s || "7" === s || "8" === s) { if (r <= "376" && "no" === o) var l = "calc(100% - " + i + "px)"; else if (r <= "736" && "no" === o) l = "calc(50% - " + i + "px)"; else if ("8" === s) l = "calc(12.5% - " + i + "px)"; else if ("7" === s) l = "calc(14.28571428571429% - " + i + "px)"; else if ("6" === s) l = "calc(16.66666666666667% - " + i + "px)"; else if ("5" === s) l = "calc(20% - " + i + "px)"; else if ("4" === s) l = "calc(25% - " + i + "px)"; else if ("3" === s) l = "calc(33.33333333333333% - " + i + "px)"; else if ("2" === s) l = "calc(50% - " + i + "px)"; else if ("1" === s) l = "calc(100% - " + i + "px)"; t.css({ width: l }); var c = t.width(); t.css({ width: l, height: c, margin: a }) } else { var n = e.attr("data-ftsi-width") ? e.attr("data-ftsi-width") : "325px"; t.css({ width: n, height: n, margin: a }) } t.width() < 180 ? (jQuery(".fts-instagram-inline-block-centered .slicker-date, .fts-instagram-inline-block-centered .fts-insta-likes-comments-grab-popup").hide(), jQuery(".slicker-instagram-placeholder").addClass("fts-smallerthan-180")) : (jQuery(".fts-instagram-inline-block-centered .slicker-date, .fts-instagram-inline-block-centered .fts-insta-likes-comments-grab-popup").show(), jQuery(".slicker-instagram-placeholder, .slicker-youtube-placeholder").removeClass("fts-smallerthan-180")) } function slickremixImageResizingFacebook() { var e = jQuery(".fts-facebook-inline-block-centered"), t = jQuery(".slicker-facebook-placeholder"), s = e.attr("data-ftsi-columns"), a = e.attr("data-ftsi-margin"), i = 1 * parseFloat(a); e.width(); if ("2" === s || "3" === s) { if ("3" === s) var r = "calc(33.0777777% - " + i + "px)"; else if ("2" === s) r = "calc(49.777777% - " + i + "px)"; t.css({ width: r }); var o = t.width(); t.css({ width: r, height: o, margin: a }) } else { var l = e.attr("data-ftsi-width") ? e.attr("data-ftsi-width") : "325px"; t.css({ width: l, height: l, margin: a }) } t.width() < 180 ? (jQuery(".fts-facebook-inline-block-centered .slicker-date, .fts-facebook-inline-block-centered .fts-insta-likes-comments-grab-popup").hide(), jQuery(".slicker-facebook-placeholder").addClass("fts-smallerthan-180")) : (jQuery(".fts-facebook-inline-block-centered .slicker-date, .fts-facebook-inline-block-centered .fts-insta-likes-comments-grab-popup").show(), jQuery(".slicker-facebook-placeholder, .slicker-youtube-placeholder").removeClass("fts-smallerthan-180")) } function slickremixImageResizingFacebook2() { var e = jQuery(".fts-more-photos-2-or-3-photos a"), t = "calc(49.88888888% - 1px)"; e.css({ width: t }); var s = e.width(); e.css({ width: t, height: s, margin: "1px" }) } function slickremixImageResizingFacebook3() { var e = jQuery(".fts-more-photos-4-photos a"), t = "calc(33.192222222% - 1px)"; e.css({ width: t }); var s = e.width(); e.css({ width: t, height: s, margin: "1px" }) } function slickremixImageResizingYouTube() { var e = jQuery(".fts-youtube-inline-block-centered"), t = jQuery(".slicker-youtube-placeholder"), s = jQuery(".fts-youtube-popup-gallery"), a = jQuery(".fts-yt-large"), i = jQuery(".fts-youtube-scrollable.fts-youtube-thumbs-wrap, .fts-youtube-scrollable.fts-youtube-thumbs-wrap-left, .youtube-comments-wrap-premium, .youtube-comments-wrap.fts-youtube-thumbs-wrap-right, .youtube-comments-wrap.fts-youtube-thumbs-wrap-left"), r = e.attr("data-ftsi-columns"), o = e.attr("data-ftsi-margin"), l = 2 * parseFloat(o), c = e.width(), n = e.attr("data-ftsi-force-columns"); if ("1" === r || "2" === r || "3" === r || "4" === r || "5" === r || "6" === r) { if (c <= "376" && "no" === n) var u = "calc(100% - " + l + "px)"; else if (c <= "736" && "no" === n) u = "calc(50% - " + l + "px)"; else if ("6" === r) u = "calc(16.66666666666667% - " + l + "px)"; else if ("5" === r) u = "calc(20% - " + l + "px)"; else if ("4" === r) u = "calc(25% - " + l + "px)"; else if ("3" === r) u = "calc(33.33333333333333% - " + l + "px)"; else if ("2" === r) u = "calc(50% - " + l + "px)"; else if ("1" === r) u = "calc(100% - " + l + "px)"; var f = a.height(); i.css({ height: f + "px" }), t.css({ width: u }), s.css({ padding: o }); var m = t.width() - "150"; t.css({ width: u, height: m, margin: o }) } t.width() < 180 ? (jQuery(".slicker-youtube-placeholder").addClass("fts-youtube-smallerthan-180"), jQuery(".fts-yt-large, .fts-youtube-scrollable").css("width", "100% !important")) : jQuery(".slicker-youtube-placeholder").removeClass("fts-youtube-smallerthan-180"), jQuery(".fts-master-youtube-wrap").width() < 550 ? jQuery(".fts-yt-large, .fts-youtube-scrollable, .youtube-comments-wrap").addClass("fts-youtube-smallerthan-550-stack") : jQuery(".fts-yt-large, .fts-youtube-scrollable, .youtube-comments-wrap").removeClass("fts-youtube-smallerthan-550-stack") } jQuery(document).ready(function () { jQuery(".fts-youtube-scrollable, .youtube-comments-wrap-premium, .youtube-comments-thumbs").hover(function () { jQuery("body").css("overflow", "hidden") }, function () { jQuery("body").css("overflow", "auto") }), jQuery(document).on("keydown", function (e) { 27 === e.keyCode && (jQuery(".fts-youtube-scrollable").removeClass("fts-scrollable-function"), jQuery(".youtube-comments-thumbs").hide(), jQuery(".fts-youtube-scrollable, .fts-fb-autoscroll-loader").show(), jQuery(".fts-youtube-thumbs-gallery-master .youtube-comments-thumbs").html(""), slickremixImageResizing()) }), jQuery.fn.ftsShare = function () { jQuery(".fts-share-wrap").each(function () { var e = jQuery(this); e.find(".ft-gallery-link-popup").unbind().bind("click", function () { e.find(".ft-gallery-share-wrap").toggle() }) }) }, jQuery.fn.ftsShare && jQuery.fn.ftsShare(), navigator.userAgent.indexOf("Firefox") > 0 || jQuery(".fts-instagram-popup-half video, .fts-simple-fb-wrapper video, .fts-slicker-facebook-posts video, .fts-fluid-videoWrapper-html5 video").click(function () { jQuery(this).trigger(this.paused ? (this.paused, "play") : "pause") }), jQuery.fn.masonry && jQuery(".fts-slicker-instagram").masonry({ itemSelector: ".fts-masonry-option" }) }), jQuery.trim(jQuery(".fts-jal-fb-group-display").html()).length || jQuery(".fts-jal-fb-group-display").append('<div class="fts-facebook-add-more-posts-notice"><p>Please go to the <strong>Facebook Options</strong> page of our plugin and look for the "<strong>Change Post Limit</strong>" option and add the number <strong>7</strong> or more. You can also hide this notice on the Facebook Options page if you want.</p>If you are trying to add a Personal Facebook feed and you are seeing this message too, please note: <strong>Personal Facebook Accounts generally do not work with our plugin.</strong></div>'), jQuery(window).on("load", function () { jQuery.fn.masonry && setTimeout(function () { jQuery(".fts-pinterest-wrapper.masonry").masonry("layout") }, 200) }), jQuery(document).ready(slickremixImageResizing), jQuery(window).on("resize", slickremixImageResizing), jQuery(document).ready(slickremixImageResizingFacebook, slickremixImageResizingFacebook2, slickremixImageResizingFacebook3), jQuery(window).on("resize", slickremixImageResizingFacebook, slickremixImageResizingFacebook2, slickremixImageResizingFacebook3), jQuery(document).ready(slickremixImageResizingYouTube), jQuery(window).on("resize", slickremixImageResizingYouTube);