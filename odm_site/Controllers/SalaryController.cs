﻿using ClosedXML.Excel;
using odm_site.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class SalaryController : Controller
    {
        // GET: Salary
        DB_HREntities Dbcontext = new DB_HREntities();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string Username, string Password)
        {
            if (Username != "")
            {
                if (Password != "")
                {
                    Tbl_user_access user = Dbcontext.Tbl_user_access.Where(a => a.role == "HR" && a.username == Username && a.password == Password).FirstOrDefault();
                    if (user != null)
                    {
                        Session["user_id"] = 121;
                        return RedirectToAction("Dashboard", "Salary");
                    }
                    else
                    {
                        ViewBag.errormessage = "Incorrect User Id and Password.... Please Try Again";
                    }
                }
                else
                {
                    ViewBag.errormessage = "Please Enter Password";
                }
            }
            else
            {
                ViewBag.errormessage = "Please Enter Username";
            }
            return View();
        }
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Salary");
        }
        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult Changepassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Savepassword(string newpassword, string reenterpassword)
        {
            if (newpassword == reenterpassword)
            {
                int id = Convert.ToInt32(Session["user_id"].ToString());
                Tbl_user_access user = Dbcontext.Tbl_user_access.Where(a => a.id == id).FirstOrDefault();
                user.password = newpassword;
                Dbcontext.SaveChanges();
                TempData["SuccessMessage"] = "Changed Successfully";
            }
            else
            {
                TempData["ErrorMessage"] = "Passwords are not same... Please try again";
            }
            return RedirectToAction("Changepassword");
        }
        public class PDF_Class
        {
            public HttpPostedFileBase[] files { get; set; }
        }
        #region Department
        public ActionResult department()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            ViewBag.departmentlist = Dbcontext.Tbl_Department.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removedepartment(int id)
        {
            var ob = Dbcontext.Tbl_Department.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Department obj = Dbcontext.Tbl_Department.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("department", "Salary");
        }
        public ActionResult createoreditdepartment(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            if (id == null)
            {
                Session["did"] = null;
                ViewBag.name = "";
            }
            else
            {
                Session["did"] = id;
                Tbl_Department ob = Dbcontext.Tbl_Department.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.name;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatedepartment(string name)
        {
            if (Session["did"] == null)
            {
                Tbl_Department ob = new Tbl_Department();
                ob.name = name;
                ob.inserted_on = DateTime.Now;
                ob.modified_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_Department.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["did"]));
                Tbl_Department ob = Dbcontext.Tbl_Department.Where(a => a.id == id).FirstOrDefault();
                ob.name = name;
                ob.modified_on = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("department", "Salary");
        }

        public ActionResult DownloadDepartmentExcel()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Department_Name", typeof(string));
            validationTable.TableName = "Department_Details";
            var worksheet = oWB.AddWorksheet(validationTable);


            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Department.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("createoreditdepartment");
        }
        public ActionResult UploadDepartment(HttpPostedFileBase file)
        {

            if (file != null)
            {
                var allowedExtensions = new[] { ".xlsx", ".xls" };

                var checkextension = Path.GetExtension(file.FileName).ToLower();
                if (!allowedExtensions.Contains(checkextension))
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("CreateOrEditDepartment");
                }


                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                    //save File
                    file.SaveAs(path);
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                    OleDbConnection connection = new OleDbConnection();
                    connection.ConnectionString = excelConnectionString;
                    OleDbDataAdapter command = new OleDbDataAdapter("select * from [Department_Details$]", connection);
                    // connection.Open();
                    // Create DbDataReader to Data Worksheet
                    DataTable dt = new DataTable();
                    command.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dt.Rows)
                        {
                            string Department_Name = dr["Department_Name"].ToString();
                            if (Dbcontext.Tbl_Department.Any(x => x.name == Department_Name.Trim() && x.is_active == true))
                            {
                                TempData["ErrorMessage"] += dr["Department_Name"].ToString() + " Already Exists.";
                            }
                            else
                            {
                                Tbl_Department s = new Tbl_Department();
                                s.name = dr["Department_Name"].ToString();
                                s.inserted_on = DateTime.Now;
                                s.modified_on = DateTime.Now;
                                s.is_active = true;
                                Dbcontext.Tbl_Department.Add(s);
                                Dbcontext.SaveChanges();
                            }
                        }
                    }
                    if ((System.IO.File.Exists(path)))
                    {
                        System.IO.File.Delete(path);
                    }
                }

                TempData["SuccessMessage"] = "Department Details Entered Successfully";
                return RedirectToAction("department");
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("createoreditdepartment");
            }
        }
        #endregion
        #region Organisation
        public ActionResult organisation()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            ViewBag.Organisationlist = Dbcontext.Tbl_Organisation.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removeorganisation(int id)
        {
            var ob = Dbcontext.Tbl_Organisation.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Organisation obj = Dbcontext.Tbl_Organisation.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("organisation", "Salary");
        }
        public ActionResult createoreditorganisation(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            if (id == null)
            {
                Session["oid"] = null;
                ViewBag.name = "";
            }
            else
            {
                Session["oid"] = id;
                Tbl_Organisation ob = Dbcontext.Tbl_Organisation.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.name;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateorganisation(string name)
        {
            if (Session["oid"] == null)
            {
                Tbl_Organisation ob = new Tbl_Organisation();
                ob.name = name;
                ob.inserted_on = DateTime.Now;
                ob.modified_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_Organisation.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["oid"]));
                Tbl_Organisation ob = Dbcontext.Tbl_Organisation.Where(a => a.id == id).FirstOrDefault();
                ob.name = name;
                ob.modified_on = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("organisation", "Salary");
        }

        public ActionResult DownloadorganisationExcel()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Organisation_Name", typeof(string));
            validationTable.TableName = "Organisation_Details";
            var worksheet = oWB.AddWorksheet(validationTable);


            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Organisation.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("createoreditorganisation");
        }
        public ActionResult Uploadorganisation(HttpPostedFileBase file)
        {

            if (file != null)
            {
                var allowedExtensions = new[] { ".xlsx", ".xls" };

                var checkextension = Path.GetExtension(file.FileName).ToLower();
                if (!allowedExtensions.Contains(checkextension))
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("createoreditorganisation");
                }


                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                    //save File
                    file.SaveAs(path);
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                    OleDbConnection connection = new OleDbConnection();
                    connection.ConnectionString = excelConnectionString;
                    OleDbDataAdapter command = new OleDbDataAdapter("select * from [Organisation_Details$]", connection);
                    // connection.Open();
                    // Create DbDataReader to Data Worksheet
                    DataTable dt = new DataTable();
                    command.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dt.Rows)
                        {
                            string Organisation_Name = dr["Organisation_Name"].ToString();
                            if (Dbcontext.Tbl_Organisation.Any(x => x.name == Organisation_Name.Trim() && x.is_active == true))
                            {
                                TempData["ErrorMessage"] += dr["Organisation_Name"].ToString() + " Already Exists.";
                            }
                            else
                            {
                                Tbl_Organisation s = new Tbl_Organisation();
                                s.name = dr["Organisation_Name"].ToString();
                                s.inserted_on = DateTime.Now;
                                s.modified_on = DateTime.Now;
                                s.is_active = true;
                                Dbcontext.Tbl_Organisation.Add(s);
                                Dbcontext.SaveChanges();
                            }
                        }
                    }
                    if ((System.IO.File.Exists(path)))
                    {
                        System.IO.File.Delete(path);
                    }
                }

                TempData["SuccessMessage"] = "Organisation Details Entered Successfully";
                return RedirectToAction("organisation");
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("createoreditorganisation");
            }
        }
        #endregion
        #region Employee Type
        public ActionResult employeetype()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            ViewBag.employeetypelist = Dbcontext.Tbl_Employee_Type.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removeemployeetype(int id)
        {
            var ob = Dbcontext.Tbl_Employee_Type.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Employee_Type obj = Dbcontext.Tbl_Employee_Type.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("employeetype", "Salary");
        }
        public ActionResult createoreditemployeetype(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            if (id == null)
            {
                Session["etid"] = null;
                ViewBag.name = "";
            }
            else
            {
                Session["etid"] = id;
                Tbl_Employee_Type ob = Dbcontext.Tbl_Employee_Type.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.name;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateemployeetype(string name)
        {
            if (Session["etid"] == null)
            {
                Tbl_Employee_Type ob = new Tbl_Employee_Type();
                ob.name = name;
                ob.inserted_on = DateTime.Now;
                ob.modified_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_Employee_Type.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["etid"]));
                Tbl_Employee_Type ob = Dbcontext.Tbl_Employee_Type.Where(a => a.id == id).FirstOrDefault();
                ob.name = name;
                ob.modified_on = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("employeetype", "Salary");
        }

        public ActionResult DownloademployeetypeExcel()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Employee_Type", typeof(string));
            validationTable.TableName = "Employee_Type_Details";
            var worksheet = oWB.AddWorksheet(validationTable);


            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Employee_Type_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("createoreditemployeetype");
        }
        public ActionResult Uploademployeetype(HttpPostedFileBase file)
        {

            if (file != null)
            {
                var allowedExtensions = new[] { ".xlsx", ".xls" };

                var checkextension = Path.GetExtension(file.FileName).ToLower();
                if (!allowedExtensions.Contains(checkextension))
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("createoreditemployeetype");
                }


                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                    //save File
                    file.SaveAs(path);
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                    OleDbConnection connection = new OleDbConnection();
                    connection.ConnectionString = excelConnectionString;
                    OleDbDataAdapter command = new OleDbDataAdapter("select * from [Employee_Type_Details$]", connection);
                    // connection.Open();
                    // Create DbDataReader to Data Worksheet
                    DataTable dt = new DataTable();
                    command.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dt.Rows)
                        {
                            string Employee_Type = dr["Employee_Type"].ToString();
                            if (Dbcontext.Tbl_Employee_Type.Any(x => x.name == Employee_Type.Trim() && x.is_active == true))
                            {
                                TempData["ErrorMessage"] += dr["Employee_Type"].ToString() + " Already Exists.";
                            }
                            else
                            {
                                Tbl_Employee_Type s = new Tbl_Employee_Type();
                                s.name = dr["Employee_Type"].ToString();
                                s.inserted_on = DateTime.Now;
                                s.modified_on = DateTime.Now;
                                s.is_active = true;
                                Dbcontext.Tbl_Employee_Type.Add(s);
                                Dbcontext.SaveChanges();
                            }
                        }
                    }
                    if ((System.IO.File.Exists(path)))
                    {
                        System.IO.File.Delete(path);
                    }
                }

                TempData["SuccessMessage"] = "Employee Type Details Entered Successfully";
                return RedirectToAction("employeetype");
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("createoreditemployeetype");
            }
        }
        #endregion
        #region Salary Category
        public ActionResult salarycategory()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            ViewBag.salarycategorylist = Dbcontext.Tbl_Salary_Category.Where(a => a.is_active == true).ToList().OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removesalarycategory(int id)
        {
            var ob = Dbcontext.Tbl_Salary_Category.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Salary_Category obj = Dbcontext.Tbl_Salary_Category.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("salarycategory", "Salary");
        }
        public ActionResult createoreditsalarycategory(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            if (id == null)
            {
                Session["scid"] = null;
                ViewBag.name = "";
            }
            else
            {
                Session["scid"] = id;
                Tbl_Salary_Category ob = Dbcontext.Tbl_Salary_Category.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.name;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatesalarycategory(string name)
        {
            if (Session["scid"] == null)
            {
                Tbl_Salary_Category ob = new Tbl_Salary_Category();
                ob.name = name;
                ob.inserted_on = DateTime.Now;
                ob.modified_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_Salary_Category.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["scid"]));
                Tbl_Salary_Category ob = Dbcontext.Tbl_Salary_Category.Where(a => a.id == id).FirstOrDefault();
                ob.name = name;
                ob.modified_on = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("salarycategory", "Salary");
        }

        #endregion

        #region Salary Category Details
        public class salarycategoryclass
        {
            public int id { get; set; }
            public string name { get; set; }
            public string category { get; set; }
            public string employeeType { get; set; }
            public Nullable<DateTime> inserted_on { get; set; }
        }
        public ActionResult salarycategorydetails()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            var categorylist = Dbcontext.Tbl_Salary_Category.Where(a => a.is_active == true).ToList();
            var catgdetailslist = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.is_active == true).ToList();
            var typelist = Dbcontext.Tbl_Employee_Type.ToList();
            var res = (from a in categorylist
                       join b in catgdetailslist on a.id equals b.salary_category_id
                       select new salarycategoryclass
                       {
                           category = a.name,
                           name = b.name,
                           id = b.id,
                           inserted_on = b.inserted_on,
                           employeeType = b.type_id == null ? "" : typelist.Where(m => m.id == b.type_id).FirstOrDefault().name
                       }).ToList();
            ViewBag.salarycategorydetailslist = res.OrderByDescending(a => a.inserted_on);
            return View();
        }
        public ActionResult removesalarycategorydetails(int id)
        {
            var ob = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Salary_Category_Details obj = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("salarycategorydetails", "Salary");
        }
        public ActionResult createoreditsalarycategorydetails(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            if (id == null)
            {
                Session["scdid"] = null;
                ViewBag.name = "";
                ViewBag.categoryid = 0;
                ViewBag.typeid = 0;
            }
            else
            {
                Session["scdid"] = id;
                Tbl_Salary_Category_Details ob = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.id == id).FirstOrDefault();
                ViewBag.name = ob.name;
                ViewBag.categoryid = ob.salary_category_id;
                ViewBag.typeid = ob.type_id;
            }
            ViewBag.typelist = Dbcontext.Tbl_Employee_Type.Where(a => a.is_active == true).ToList();
            ViewBag.schoolcategorylist = Dbcontext.Tbl_Salary_Category.Where(a => a.is_active == true).ToList();
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdatesalarycategorydetails(string name, int catgid, int type)
        {
            if (Session["scdid"] == null)
            {
                Tbl_Salary_Category_Details ob = new Tbl_Salary_Category_Details();
                ob.name = name;
                ob.salary_category_id = catgid;
                ob.type_id = type;
                ob.inserted_on = DateTime.Now;
                ob.modified_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_Salary_Category_Details.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["scdid"]));
                Tbl_Salary_Category_Details ob = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.id == id).FirstOrDefault();
                ob.name = name;
                ob.type_id = type;
                ob.salary_category_id = catgid;
                ob.modified_on = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("salarycategorydetails", "Salary");
        }
        public ActionResult Downloadsalarycategory()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();

            //create worksheet for group master table
            // CREATE A DATATABLE AND ADD DATA FROM TABLE TO DATATABLE
            DataTable gdt = new DataTable("Salary_Category");
            gdt.Columns.Add("Id");
            gdt.Columns.Add("Type");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var g = Dbcontext.Tbl_Salary_Category.Where(a => a.is_active == true).ToList();
            foreach (var gs in g.ToList())
            {
                DataRow dr = gdt.NewRow();
                dr["Id"] = gs.id;
                dr["Type"] = gs.name;
                gdt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo1 = gdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet1 = oWB.Worksheet(1);


            DataTable tdt = new DataTable("Employee_Type");
            tdt.Columns.Add("Type");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var t = Dbcontext.Tbl_Employee_Type.Where(a => a.is_active == true).ToList();
            foreach (var gs in t.ToList())
            {
                DataRow dr = tdt.NewRow();
                dr["Type"] = gs.name;
                tdt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo2 = tdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(tdt);
            var worksheet2 = oWB.Worksheet(2);

            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Employee_Type");
            validationTable.Columns.Add("Type");
            validationTable.Columns.Add("Category_Name");
            validationTable.TableName = "Salary_Category_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            //ADD DATA VALIDATION IN MATERIAL WORKSHEET. FOR DROPDOWN LIST SHOWN IN EXCEL SHEET...
            worksheet.Column(2).SetDataValidation().List(worksheet1.Range("B2:B" + lastCellNo1), true);
            worksheet.Column(1).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet1.Hide();
            worksheet2.Hide();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Salary_Category_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }

            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("salarycategorydetails");
        }
        public ActionResult Uploadsalarycategory(HttpPostedFileBase file)
        {

            if (file != null)
            {
                var allowedExtensions = new[] { ".xlsx", ".xls" };

                var checkextension = Path.GetExtension(file.FileName).ToLower();
                if (!allowedExtensions.Contains(checkextension))
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("createoreditsalarycategorydetails");
                }


                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                    //save File
                    file.SaveAs(path);
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                    OleDbConnection connection = new OleDbConnection();
                    connection.ConnectionString = excelConnectionString;
                    OleDbDataAdapter command = new OleDbDataAdapter("select * from [Salary_Category_Details$]", connection);
                    // connection.Open();
                    // Create DbDataReader to Data Worksheet
                    DataTable dt = new DataTable();
                    command.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["Employee_Type"] != null)
                            {
                                string type = dr["Type"].ToString();
                                string Category_Name = dr["Category_Name"].ToString();
                                string Employee_Type = dr["Employee_Type"].ToString();
                                var etypeid = Dbcontext.Tbl_Employee_Type.Where(a => a.name == Employee_Type).FirstOrDefault().id;
                                var typeid = Dbcontext.Tbl_Salary_Category.Where(a => a.name == type).FirstOrDefault().id;
                                if (Dbcontext.Tbl_Salary_Category_Details.Any(x => x.name == Category_Name.Trim() && x.salary_category_id == typeid && x.type_id == etypeid && x.is_active == true))
                                {
                                    TempData["ErrorMessage"] += dr["Category_Name"].ToString() + " Already Exists.";
                                }
                                else
                                {
                                    Tbl_Salary_Category_Details ob = new Tbl_Salary_Category_Details();
                                    ob.name = dr["Category_Name"].ToString();
                                    ob.salary_category_id = typeid;
                                    ob.type_id = etypeid;
                                    ob.inserted_on = DateTime.Now;
                                    ob.modified_on = DateTime.Now;
                                    ob.is_active = true;
                                    Dbcontext.Tbl_Salary_Category_Details.Add(ob);
                                    Dbcontext.SaveChanges();
                                }
                            }
                        }
                    }
                    if ((System.IO.File.Exists(path)))
                    {
                        System.IO.File.Delete(path);
                    }
                }

                TempData["SuccessMessage"] = "Salary Category Details Entered Successfully";
                return RedirectToAction("salarycategorydetails");
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("createoreditsalarycategorydetails");
            }
        }
        #endregion

        #region Employee
        public class Employeeclass
        {
            public int? id { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public string Mobile { get; set; }
            public string MailId { get; set; }
            public string Designation { get; set; }
            public string Organisation { get; set; }
            public string employeetype { get; set; }
            public string Department { get; set; }
        }
        public ActionResult Employeedetails()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            var st = Dbcontext.Tbl_Employee.Where(a => a.is_active == true).ToList();
            var ct = Dbcontext.Tbl_Department.ToList();
            var org = Dbcontext.Tbl_Organisation.ToList();
            var et = Dbcontext.Tbl_Employee_Type.Where(a => a.is_active == true).ToList();
            var res = (from a in st
                       select new Employeeclass
                       {
                           id = a.id,
                           Code = a.emp_code,
                           Name = a.emp_name,
                           Mobile = a.phone,
                           MailId = a.email_id,
                           Designation = a.designation,
                           Department = a.dept_id == null ? "" : ct.Where(m => m.id == a.dept_id).FirstOrDefault().name,
                           Organisation = a.organisation_id == null ? "" : org.Where(m => m.id == a.organisation_id).FirstOrDefault().name,
                           employeetype = a.type_id == null ? "" : et.Where(m => m.id == a.type_id).FirstOrDefault().name
                       }).ToList();
            ViewBag.EmployeeList = res;
            return View();
        }
        public ActionResult removeEmployee(int id)
        {
            var ob = Dbcontext.Tbl_Employee.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Employee obj = Dbcontext.Tbl_Employee.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Employeedetails", "Salary");
        }
        public ActionResult createoreditEmployee(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            if (id == null)
            {
                Session["eid"] = null;
                ViewBag.emp_name = "";
                ViewBag.dept_id = 0;
                ViewBag.designation = "";
                ViewBag.email_id = "";
                ViewBag.emp_code = "";
                ViewBag.phone = "";
                ViewBag.organisation_id = 0;
                ViewBag.type_id = 0;
            }
            else
            {
                Session["eid"] = id;
                Tbl_Employee ob = Dbcontext.Tbl_Employee.Where(a => a.id == id).FirstOrDefault();
                ViewBag.emp_name = ob.emp_name;
                ViewBag.dept_id = ob.dept_id;
                ViewBag.designation = ob.designation;
                ViewBag.email_id = ob.email_id;
                ViewBag.emp_code = ob.emp_code;
                ViewBag.phone = ob.phone;
                ViewBag.organisation_id = ob.organisation_id;
                ViewBag.type_id = ob.type_id;
            }
            ViewBag.departmentlist = Dbcontext.Tbl_Department.Where(a => a.is_active == true).ToList();
            ViewBag.organisationlist = Dbcontext.Tbl_Organisation.Where(a => a.is_active == true).ToList();
            ViewBag.employeetypelist = Dbcontext.Tbl_Employee_Type.Where(a => a.is_active == true).ToList();
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateEmployee(string name, int deptid, string designation, string email, string code, string phone, int organisation, int type)
        {
            if (Session["eid"] == null)
            {
                Tbl_Employee ob = new Tbl_Employee();
                ob.emp_name = name;
                ob.emp_code = code;
                ob.dept_id = deptid;
                ob.designation = designation;
                ob.email_id = email;
                ob.phone = phone;
                ob.organisation_id = organisation;
                ob.type_id = type;
                ob.inserted_on = DateTime.Now;
                ob.modified_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_Employee.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["eid"]));
                Tbl_Employee ob = Dbcontext.Tbl_Employee.Where(a => a.id == id).FirstOrDefault();
                ob.emp_name = name;
                ob.emp_code = code;
                ob.dept_id = deptid;
                ob.designation = designation;
                ob.email_id = email;
                ob.phone = phone;
                ob.organisation_id = organisation;
                ob.type_id = type;
                ob.modified_on = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("Employeedetails", "Salary");
        }

        public ActionResult DownloadEmployee()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            DataTable gdt = new DataTable("Department");
            gdt.Columns.Add("Name");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var g = Dbcontext.Tbl_Department.Where(a => a.is_active == true).ToList();
            foreach (var gs in g.ToList())
            {
                DataRow dr = gdt.NewRow();
                dr["Name"] = gs.name;
                gdt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo1 = gdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable odt = new DataTable("Organisation");
            odt.Columns.Add("Name");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var o = Dbcontext.Tbl_Organisation.Where(a => a.is_active == true).ToList();
            foreach (var gs in o.ToList())
            {
                DataRow dr = odt.NewRow();
                dr["Name"] = gs.name;
                odt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo2 = odt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(odt);
            var worksheet2 = oWB.Worksheet(2);
            DataTable edt = new DataTable("EmployeeType");
            edt.Columns.Add("Name");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var et = Dbcontext.Tbl_Employee_Type.Where(a => a.is_active == true).ToList();
            foreach (var gs in et.ToList())
            {
                DataRow dr = edt.NewRow();
                dr["Name"] = gs.name;
                edt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo3 = edt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(edt);
            var worksheet3 = oWB.Worksheet(3);




            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Employee_Code", typeof(string));
            validationTable.Columns.Add("Employee_Name", typeof(string));
            validationTable.Columns.Add("Department", typeof(string));
            validationTable.Columns.Add("Designation", typeof(string));
            validationTable.Columns.Add("Organisation", typeof(string));
            validationTable.Columns.Add("Employee_Type", typeof(string));
            validationTable.Columns.Add("Email", typeof(string));
            validationTable.Columns.Add("Phone", typeof(string));
            validationTable.TableName = "Employee_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            worksheet.Column(3).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(5).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(6).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Employee_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("Employeedetails");
        }
        public ActionResult UploadEmployee(HttpPostedFileBase file)
        {

            if (file != null)
            {
                var allowedExtensions = new[] { ".xlsx", ".xls" };

                var checkextension = Path.GetExtension(file.FileName).ToLower();
                if (!allowedExtensions.Contains(checkextension))
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("createoreditEmployee");
                }


                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                    //save File
                    file.SaveAs(path);
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                    OleDbConnection connection = new OleDbConnection();
                    connection.ConnectionString = excelConnectionString;
                    OleDbDataAdapter command = new OleDbDataAdapter("select * from [Employee_Details$]", connection);
                    // connection.Open();
                    // Create DbDataReader to Data Worksheet
                    DataTable dt = new DataTable();
                    command.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dt.Rows)
                        {
                            string type = dr["Department"].ToString();
                            string Phone = dr["Phone"].ToString();
                            string organisation = dr["Organisation"].ToString();
                            string employeetype = dr["Employee_Type"].ToString();
                            var orgid = Dbcontext.Tbl_Organisation.Where(a => a.name == organisation).FirstOrDefault().id;
                            var etypeid = Dbcontext.Tbl_Employee_Type.Where(a => a.name == employeetype).FirstOrDefault().id;
                            var typeid = Dbcontext.Tbl_Department.Where(a => a.name == type).FirstOrDefault().id;
                            if (Dbcontext.Tbl_Employee.Any(x => x.phone == Phone.Trim() && x.is_active == true))
                            {
                                TempData["ErrorMessage"] += dr["Employee_Name"].ToString() + " Already Exists.";
                            }
                            else
                            {
                                Tbl_Employee ob = new Tbl_Employee();
                                ob.emp_name = dr["Employee_Name"].ToString();
                                ob.emp_code = dr["Employee_Code"].ToString();
                                ob.dept_id = typeid;
                                ob.designation = dr["Designation"].ToString();
                                ob.email_id = dr["Email"].ToString();
                                ob.phone = dr["Phone"].ToString();
                                ob.organisation_id = orgid;
                                ob.type_id = etypeid;
                                ob.inserted_on = DateTime.Now;
                                ob.modified_on = DateTime.Now;
                                ob.is_active = true;
                                Dbcontext.Tbl_Employee.Add(ob);
                                Dbcontext.SaveChanges();
                            }
                        }
                    }
                    if ((System.IO.File.Exists(path)))
                    {
                        System.IO.File.Delete(path);
                    }
                }

                TempData["SuccessMessage"] = "Salary Category Details Entered Successfully";
                return RedirectToAction("Employeedetails");
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("createoreditEmployee");
            }
        }
        #endregion

        #region Employee Salary
        public class employeesalarydetailsclass
        {
            public int id { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public string Mobile { get; set; }
            public string MailId { get; set; }
            public string Designation { get; set; }
            public string Department { get; set; }
            public string monthyear { get; set; }
            public decimal totalSalary { get; set; }
            public decimal netdeduction { get; set; }
            public decimal netsalary { get; set; }

        }
        public ActionResult EmployeeSalarydetails()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            var salary = Dbcontext.Tbl_Employee_Salary.Where(a => a.is_active == true).ToList();
            var st = Dbcontext.Tbl_Employee.Where(a => a.is_active == true).ToList();
            var ct = Dbcontext.Tbl_Department.Where(a => a.is_active == true).ToList();
            var res = (from a in salary
                       join b in st on a.emp_id equals b.id
                       select new employeesalarydetailsclass
                       {
                           id = a.id,
                           Code = b.emp_code,
                           Name = b.emp_name,
                           Mobile = b.phone,
                           MailId = b.email_id,
                           Designation = b.designation,
                           Department = b.dept_id == null ? "" : ct.Where(m => m.id == b.dept_id).FirstOrDefault().name,
                           monthyear = DateTimeFormatInfo.InvariantInfo.MonthNames[a.month.Value - 1] + " " + a.year.Value,
                           netdeduction = a.net_deduction.Value,
                           netsalary = a.net_salary.Value,
                           totalSalary = a.total_salary.Value
                       }).ToList().OrderByDescending(a => a.id);
            ViewBag.EmployeesalaryList = res;
            ViewBag.month = null;
            ViewBag.year = null;
            return View();
        }
        public ActionResult EmployeeSalarydetailsbymonthandyear(int? month, int? year)
        {
            if (month == 0 || year == 0 || month == null || year == null)
            {
                return RedirectToAction("EmployeeSalarydetails");
            }
            else
            {
                if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
                {
                    return RedirectToAction("Index", "Salary");
                }
                var salary = Dbcontext.Tbl_Employee_Salary.Where(a => a.is_active == true).ToList();
                var st = Dbcontext.Tbl_Employee.Where(a => a.is_active == true).ToList();
                var ct = Dbcontext.Tbl_Department.Where(a => a.is_active == true).ToList();
                var res = (from a in salary
                           join b in st on a.emp_id equals b.id
                           where a.month == month && a.year == year
                           select new employeesalarydetailsclass
                           {
                               id = a.id,
                               Code = b.emp_code,
                               Name = b.emp_name,
                               Mobile = b.phone,
                               MailId = b.email_id,
                               Designation = b.designation,
                               Department = b.dept_id == null ? "" : ct.Where(m => m.id == b.dept_id).FirstOrDefault().name,
                               monthyear = DateTimeFormatInfo.InvariantInfo.MonthNames[a.month.Value - 1] + " " + a.year.Value,
                               netdeduction = a.net_deduction.Value,
                               netsalary = a.net_salary.Value,
                               totalSalary = a.total_salary.Value
                           }).ToList().OrderByDescending(a => a.id);
                ViewBag.EmployeesalaryList = res;
                ViewBag.month = month - 1;
                ViewBag.year = year;
                return View("EmployeeSalarydetails");
            }
        }
        public ActionResult removeEmployeeSalary(int id)
        {
            var ob = Dbcontext.Tbl_Employee_Salary.Where(a => a.id == id).ToList();
            if (ob.Count > 0)
            {
                Tbl_Employee_Salary obj = Dbcontext.Tbl_Employee_Salary.Where(a => a.id == id).FirstOrDefault();
                obj.is_active = false;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("EmployeeSalarydetails", "Salary");
        }
        public class salarycategorydetailsclass
        {
            public int categoryid { get; set; }
            public string categoryname { get; set; }
            public string categorydetailsname { get; set; }
            public int categorydetailsid { get; set; }
            public Nullable<decimal> amount { get; set; }
        }
        public class EmployeeSalaryClass
        {
            public int emp_id { get; set; }
            public string emp_code { get; set; }
            public string empname { get; set; }
            public string department { get; set; }
            public string designation { get; set; }
            public string email_id { get; set; }
            public string phone { get; set; }
            public string organisation { get; set; }
            public Nullable<decimal> totalsalary { get; set; }
            public Nullable<decimal> netsalary { get; set; }
            public Nullable<decimal> netdeduction { get; set; }
            public int? month { get; set; }
            public int? year { get; set; }
            public string type { get; set; }
            public List<salarycategorydetailsclass> salarycatgdetails { get; set; }
        }
        public ActionResult createoreditEmployeeSalary(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("Index", "Salary");
            }
            if (id == null)
            {
                Session["emid"] = null;

                EmployeeSalaryClass mn = new EmployeeSalaryClass();
                mn.emp_id = 0;
                mn.month = DateTime.Now.Month;
                mn.year = DateTime.Now.Year;
                mn.department = "";
                mn.designation = "";
                mn.email_id = "";
                mn.emp_code = "";
                mn.empname = "";
                mn.netdeduction = 0;
                mn.netsalary = 0;
                mn.phone = "";
                mn.totalsalary = 0;
                mn.type = "";
                mn.organisation = "";
                mn.salarycatgdetails = null;
                ViewBag.salarydetailsdata = mn;
            }
            else
            {
                Session["emid"] = id;
                Tbl_Employee_Salary ob = Dbcontext.Tbl_Employee_Salary.Where(a => a.id == id).FirstOrDefault();
                var obdt = Dbcontext.Tbl_Employee_Salary_Details.Where(a => a.emp_salary_id == id && a.is_active == true).ToList();
                Tbl_Employee ob1 = Dbcontext.Tbl_Employee.Where(a => a.id == ob.emp_id).FirstOrDefault();
                var categorylist = Dbcontext.Tbl_Salary_Category.Where(a => a.is_active == true).ToList();
                var catgdetailslist = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.is_active == true).ToList();
                var res = (from a in categorylist
                           join b in catgdetailslist on a.id equals b.salary_category_id
                           where b.type_id == ob1.type_id
                           select new salarycategorydetailsclass
                           {
                               categoryid = a.id,
                               categoryname = a.name,
                               categorydetailsname = b.name,
                               categorydetailsid = b.id,
                               amount = obdt.Where(m => m.emp_category_details_id == b.id).FirstOrDefault().amount
                           }).ToList();
                EmployeeSalaryClass mn = new EmployeeSalaryClass();
                mn.department = ob1.dept_id != null ? Dbcontext.Tbl_Department.Where(a => a.id == ob1.dept_id).FirstOrDefault().name : "";
                mn.designation = ob1.designation;
                mn.email_id = ob1.email_id;
                mn.emp_code = ob1.emp_code;
                mn.emp_id = ob1.id;
                mn.empname = ob1.emp_name;
                mn.netdeduction = ob.net_deduction;
                mn.netsalary = ob.net_salary;
                mn.phone = ob1.phone;
                mn.totalsalary = ob.total_salary;
                mn.salarycatgdetails = res;
                mn.year = ob.year;
                mn.month = ob.month;
                mn.organisation = ob1.organisation_id == null ? "" : Dbcontext.Tbl_Organisation.Where(a => a.id == ob1.organisation_id).FirstOrDefault().name;
                mn.type = ob1.type_id == null ? "" : Dbcontext.Tbl_Employee_Type.Where(a => a.id == ob1.type_id).FirstOrDefault().name;
                ViewBag.salarydetailsdata = mn;
            }
            ViewBag.employeelist = Dbcontext.Tbl_Employee.Where(a => a.is_active == true).ToList();
            ViewBag.departmentlist = Dbcontext.Tbl_Department.Where(a => a.is_active == true).ToList();
            ViewBag.typelist = Dbcontext.Tbl_Employee_Type.Where(a => a.is_active == true).ToList();
            return View();
        }
        public JsonResult GetEmployeeDetails(int id)
        {
            Tbl_Employee ob1 = Dbcontext.Tbl_Employee.Where(a => a.id == id).FirstOrDefault();
            if (ob1 != null)
            {
                var categorylist = Dbcontext.Tbl_Salary_Category.Where(a => a.is_active == true).ToList();
                var catgdetailslist = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.is_active == true).ToList();
                var income = (from a in categorylist
                              join b in catgdetailslist on a.id equals b.salary_category_id
                              where b.type_id == ob1.type_id && b.salary_category_id == 1
                              select new salarycategorydetailsclass
                              {
                                  categoryid = a.id,
                                  categoryname = a.name,
                                  categorydetailsname = b.name,
                                  categorydetailsid = b.id
                              }).ToList();
                var deduction = (from a in categorylist
                                 join b in catgdetailslist on a.id equals b.salary_category_id
                                 where b.type_id == ob1.type_id && b.salary_category_id == 2
                                 select new salarycategorydetailsclass
                                 {
                                     categoryid = a.id,
                                     categoryname = a.name,
                                     categorydetailsname = b.name,
                                     categorydetailsid = b.id
                                 }).ToList();
                var ob = new
                {
                    income = income,
                    deduction = deduction,
                    code = ob1.emp_code,
                    name = ob1.emp_name,
                    dept = ob1.dept_id != null ? Dbcontext.Tbl_Department.Where(a => a.id == ob1.dept_id).FirstOrDefault().name : "",
                    mobile = ob1.phone,
                    email = ob1.email_id,
                    designation = ob1.designation,
                    type = ob1.type_id == null ? "" : Dbcontext.Tbl_Employee_Type.Where(a => a.id == ob1.type_id).FirstOrDefault().name,
                    organisation = ob1.organisation_id == null ? "" : Dbcontext.Tbl_Organisation.Where(a => a.id == ob1.organisation_id).FirstOrDefault().name

                };
                return Json(ob, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public class salarydetails
        {
            public int emp_id { get; set; }
            public int month { get; set; }
            public int year { get; set; }
            public decimal netdeduction { get; set; }
            public decimal netsalary { get; set; }
            public decimal totalsalary { get; set; }
        }
        public class salarycatgamountclass
        {
            public int categorydetailsid { get; set; }
            public decimal amount { get; set; }
        }
        [HttpPost]
        public ActionResult SaveorUpdateEmployeeSalary(List<salarycatgamountclass> catg, salarydetails empsalary)
        {
            int salary_id = 0;
            if (Session["emid"] == null)
            {
                Tbl_Employee_Salary ob = new Tbl_Employee_Salary();
                ob.emp_id = empsalary.emp_id;
                ob.month = empsalary.month;
                ob.net_deduction = empsalary.netdeduction;
                ob.net_salary = empsalary.netsalary;
                ob.total_salary = empsalary.totalsalary;
                ob.year = empsalary.year;
                ob.inserted_on = DateTime.Now;
                ob.modified_on = DateTime.Now;
                ob.is_active = true;
                Dbcontext.Tbl_Employee_Salary.Add(ob);
                Dbcontext.SaveChanges();
                salary_id = ob.id;
                foreach (salarycatgamountclass sal in catg)
                {
                    Tbl_Employee_Salary_Details sd = new Tbl_Employee_Salary_Details();
                    sd.emp_salary_id = salary_id;
                    sd.emp_category_details_id = sal.categorydetailsid;
                    sd.amount = sal.amount;
                    sd.inserted_on = DateTime.Now;
                    sd.is_active = true;
                    sd.modified_on = DateTime.Now;
                    Dbcontext.Tbl_Employee_Salary_Details.Add(sd);
                    Dbcontext.SaveChanges();
                }
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["emid"]));
                salary_id = id;
                Tbl_Employee_Salary ob = Dbcontext.Tbl_Employee_Salary.Where(a => a.id == id).FirstOrDefault();
                ob.emp_id = empsalary.emp_id;
                ob.month = empsalary.month;
                ob.net_deduction = empsalary.netdeduction;
                ob.net_salary = empsalary.netsalary;
                ob.total_salary = empsalary.totalsalary;
                ob.year = empsalary.year;
                ob.modified_on = DateTime.Now;
                Dbcontext.SaveChanges();
                foreach (salarycatgamountclass sal in catg)
                {
                    Tbl_Employee_Salary_Details sd = Dbcontext.Tbl_Employee_Salary_Details.Where(a => a.emp_salary_id == id && a.emp_category_details_id == sal.categorydetailsid).FirstOrDefault();
                    if (sd == null)
                    {
                        sd = new Tbl_Employee_Salary_Details();
                        sd.emp_salary_id = id;
                        sd.emp_category_details_id = sal.categorydetailsid;
                        sd.amount = sal.amount;
                        sd.inserted_on = DateTime.Now;
                        sd.is_active = true;
                        sd.modified_on = DateTime.Now;
                        Dbcontext.Tbl_Employee_Salary_Details.Add(sd);
                        Dbcontext.SaveChanges();
                    }
                    else
                    {
                        sd.amount = sal.amount;
                        sd.modified_on = DateTime.Now;
                        sd.is_active = true;
                        Dbcontext.SaveChanges();
                    }
                }
            }
            var empid = Dbcontext.Tbl_Employee.Where(a => a.id == empsalary.emp_id).FirstOrDefault();
            string encode = Encodings.Base64EncodingMethod(salary_id.ToString());
            string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=Salaryslip&sid=" + encode;
            //bitly b = new bitly();
            string shortlink = ToTinyURLS(url);
            string org = empid.organisation_id == null ? "" : Dbcontext.Tbl_Organisation.Where(a => a.id == empid.organisation_id).FirstOrDefault().name;
            if (empid.phone != "")
            {
                Sendsmstoemployee(empid.phone, "Hi, Hope you have received the salary for the month of " + DateTimeFormatInfo.InvariantInfo.MonthNames[empsalary.month - 1] + ". Download your salary slip at- " + shortlink + " \n\n Regards\n ODM Educational Group");
            }

            if (empid.email_id != "")
            {
                string body = PopulateBody(empid.emp_name, salary_id, shortlink, DateTimeFormatInfo.InvariantInfo.MonthNames[empsalary.month - 1], org);
                SendHtmlFormattedEmail(empid.email_id, "Salary Slip | ODM Educational Group | " + DateTimeFormatInfo.InvariantInfo.MonthNames[empsalary.month - 1], body);
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("EmployeeSalarydetails", "Salary");
        }
        public ActionResult DownloadEmployeeSalary(int id)
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //DataTable gdt = new DataTable("Department");
            //gdt.Columns.Add("Id");
            //gdt.Columns.Add("Name");

            ////Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            //var g = Dbcontext.Tbl_Department.Where(a => a.is_active == true).ToList();
            //foreach (var gs in g.ToList())
            //{
            //    DataRow dr = gdt.NewRow();
            //    dr["Id"] = gs.id;
            //    dr["Name"] = gs.name;
            //    gdt.Rows.Add(dr);
            //}
            //// COUNT TABLE DATA
            //int lastCellNo1 = gdt.Rows.Count + 1;
            ////ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            //oWB.AddWorksheet(gdt);

            //var worksheet1 = oWB.Worksheet(1);
            DataTable month = new DataTable("month");
            month.Columns.Add("month");
            for (int i = 1; i <= 12; i++)
            {
                DataRow dr = month.NewRow();
                dr["month"] = DateTimeFormatInfo.InvariantInfo.MonthNames[i - 1];
                month.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo1 = month.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(month);
            var worksheet1 = oWB.Worksheet(1);

            DataTable year = new DataTable("year");
            year.Columns.Add("year");
            for (int i = 1990; i <= DateTime.Now.Year; i++)
            {
                DataRow dr = year.NewRow();
                dr["year"] = i;
                year.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo2 = year.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(year);
            var worksheet2 = oWB.Worksheet(2);


            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Employee_Code");
            validationTable.Columns.Add("Employee_Name");
            //validationTable.Columns.Add("Department");
            validationTable.Columns.Add("Month");
            validationTable.Columns.Add("Year");
            var catg = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.is_active == true && a.type_id == id).ToList();
            foreach (var item in catg)
            {
                validationTable.Columns.Add(item.name + " " + (item.salary_category_id == 1 ? "INCOME" : "DEDUCTION"));
            }
            validationTable.Columns.Add("Total_Salary");
            validationTable.Columns.Add("Net_Deduction");
            validationTable.Columns.Add("Net_Salary");
            validationTable.TableName = "Employee_Salary_Details";
            var worksheet = oWB.AddWorksheet(validationTable);


            worksheet.Column(3).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            worksheet.Column(4).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet2.Hide();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Employee_Salary_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("EmployeeSalarydetails");
        }
        public ActionResult UploadEmployeeSalary(HttpPostedFileBase file, int typeid)
        {

            if (file != null)
            {
                var allowedExtensions = new[] { ".xlsx", ".xls" };

                var checkextension = Path.GetExtension(file.FileName).ToLower();
                if (!allowedExtensions.Contains(checkextension))
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("createoreditEmployeeSalary");
                }


                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                    //save File
                    file.SaveAs(path);
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                    OleDbConnection connection = new OleDbConnection();
                    connection.ConnectionString = excelConnectionString;
                    OleDbDataAdapter command = new OleDbDataAdapter("select * from [Employee_Salary_Details$]", connection);
                    // connection.Open();
                    // Create DbDataReader to Data Worksheet
                    DataTable dt = new DataTable();
                    command.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["Employee_Code"] != "")
                            {
                                string employeename = dr["Employee_Name"].ToString().ToLower().Trim();
                                string Employee_Code = dr["Employee_Code"].ToString();
                                var empid = Dbcontext.Tbl_Employee.Where(a => a.emp_code == Employee_Code.Trim() && a.emp_name.ToLower().Trim() == employeename && a.is_active == true).FirstOrDefault();
                                if (empid == null)
                                {
                                    TempData["ErrorMessage"] += employeename + "(" + dr["Employee_Code"].ToString() + ") Doesn't Exists.";
                                }
                                else
                                {
                                    Tbl_Employee_Salary ob = new Tbl_Employee_Salary();
                                    ob.emp_id = empid.id;
                                    ob.month = DateTime.ParseExact(dr["Month"].ToString(), "MMMM", CultureInfo.InvariantCulture).Month;
                                    ob.net_deduction = Convert.ToDecimal(dr["Net_Deduction"].ToString().Trim());
                                    ob.net_salary = Convert.ToDecimal(dr["Net_Salary"].ToString().Trim());
                                    ob.total_salary = Convert.ToDecimal(dr["Total_Salary"].ToString().Trim());
                                    ob.year = Convert.ToInt32(dr["Year"].ToString().Trim());
                                    ob.inserted_on = DateTime.Now;
                                    ob.modified_on = DateTime.Now;
                                    ob.is_active = true;
                                    Dbcontext.Tbl_Employee_Salary.Add(ob);
                                    Dbcontext.SaveChanges();
                                    int salary_id = ob.id;
                                    var catg = Dbcontext.Tbl_Salary_Category_Details.Where(a => a.is_active == true && a.type_id == typeid).ToList();
                                    List<salarycatgamountclass> salarycatg = new List<salarycatgamountclass>();
                                    foreach (var item in catg)
                                    {

                                        Tbl_Employee_Salary_Details sd = new Tbl_Employee_Salary_Details();
                                        sd.emp_salary_id = salary_id;
                                        sd.emp_category_details_id = item.id;
                                        sd.amount = dr[(item.name + " " + (item.salary_category_id == 1 ? "INCOME" : "DEDUCTION")).Replace('.', '#')].ToString().Trim() == "" ? 0 : Convert.ToDecimal(dr[(item.name + " " + (item.salary_category_id == 1 ? "INCOME" : "DEDUCTION")).Replace('.', '#')].ToString().Trim());
                                        sd.inserted_on = DateTime.Now;
                                        sd.is_active = true;
                                        sd.modified_on = DateTime.Now;
                                        Dbcontext.Tbl_Employee_Salary_Details.Add(sd);
                                        Dbcontext.SaveChanges();
                                    }
                                    string encode = Encodings.Base64EncodingMethod(salary_id.ToString());
                                    string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=Salaryslip&sid=" + encode;
                                    //bitly b = new bitly();
                                    string shortlink = ToTinyURLS(url);
                                    string org = empid.organisation_id == null ? "" : Dbcontext.Tbl_Organisation.Where(a => a.id == empid.organisation_id).FirstOrDefault().name;
                                    if (empid.phone != "")
                                    {
                                        Sendsmstoemployee(empid.phone, "Hi, Hope you have received the salary for the month of " + dr["Month"].ToString() + ". Download your salary slip at- " + shortlink + " \n\n Regards\n ODM Educational Group");
                                    }

                                    if (empid.email_id != "")
                                    {
                                        string body = PopulateBody(empid.emp_name, salary_id, shortlink, dr["Month"].ToString(), org);
                                        SendHtmlFormattedEmail(empid.email_id, "Salary Slip | ODM Educational Group | " + dr["Month"].ToString(), body);
                                    }
                                }
                            }
                        }
                    }
                    if ((System.IO.File.Exists(path)))
                    {
                        System.IO.File.Delete(path);
                    }
                }

                TempData["SuccessMessage"] = "Employee Salary Details Entered Successfully";
                return RedirectToAction("EmployeeSalarydetails");
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("createoreditEmployeeSalary");
            }
        }

        public ActionResult SendSalarySlipByID(int id)
        {
            Tbl_Employee_Salary ob = Dbcontext.Tbl_Employee_Salary.Where(a => a.id == id).FirstOrDefault();
            Tbl_Employee ob1 = Dbcontext.Tbl_Employee.Where(a => a.id == ob.emp_id).FirstOrDefault();
            string encode = Encodings.Base64EncodingMethod(id.ToString());
            string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=Salaryslip&sid=" + encode;
            //bitly b = new bitly();
            string shortlink = ToTinyURLS(url);
            if (ob1.phone != "")
            {
                Sendsmstoemployee(ob1.phone, "Hi, Hope you have received the salary for the month of " + DateTimeFormatInfo.InvariantInfo.MonthNames[ob.month.Value - 1] + ". Download your salary slip at- " + shortlink + " \n\n Regards\n ODM Educational Group");
            }
            string org = ob1.organisation_id == null ? "" : Dbcontext.Tbl_Organisation.Where(a => a.id == ob1.organisation_id).FirstOrDefault().name;
            if (ob1.email_id != "")
            {
                string body = PopulateBody(ob1.emp_name, id, shortlink, DateTimeFormatInfo.InvariantInfo.MonthNames[ob.month.Value - 1], org);
                SendHtmlFormattedEmail(ob1.email_id, "Salary Slip | ODM Educational Group | " + DateTimeFormatInfo.InvariantInfo.MonthNames[ob.month.Value - 1], body);
            }
            return RedirectToAction("EmployeeSalarydetails");
        }
        public JsonResult SendSalarySlip(int month, int year)
        {
            try
            {
                var ob = Dbcontext.Tbl_Employee_Salary.Where(a => a.month == month && a.year == year && a.is_active == true).ToList();
                foreach (var item in ob)
                {
                    Tbl_Employee ob1 = Dbcontext.Tbl_Employee.Where(a => a.id == item.emp_id).FirstOrDefault();
                    string encode = Encodings.Base64EncodingMethod(item.id.ToString());
                    string org = ob1.organisation_id == null ? "" : Dbcontext.Tbl_Organisation.Where(a => a.id == ob1.organisation_id).FirstOrDefault().name;
                    string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=Salaryslip&sid=" + encode;
                    string shortlink = ToTinyURLS(url);
                    if (ob1.phone != "")
                    {
                        Sendsmstoemployee(ob1.phone, "Hi, Hope you have received the salary for the month of " + DateTimeFormatInfo.InvariantInfo.MonthNames[item.month.Value - 1] + ". Download your salary slip at- " + shortlink + " \n\n Regards\n ODM Educational Group");
                    }
                    if (ob1.email_id != "")
                    {
                        string body = PopulateBody(ob1.emp_name, item.id, shortlink, DateTimeFormatInfo.InvariantInfo.MonthNames[item.month.Value - 1], org);
                        SendHtmlFormattedEmail(ob1.email_id, "Salary Slip | ODM Educational Group | " + DateTimeFormatInfo.InvariantInfo.MonthNames[item.month.Value - 1], body);
                    }
                }
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Bitly

        protected string ToTinyURLS(string txt)
        {
            Regex regx = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                string tURL = MakeTinyUrl(match.Value);
                txt = txt.Replace(match.Value, tURL);
            }

            return txt;
        }

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 12)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return Url;
            }
        }

        #endregion
        #region Mail and Messages

        public bool Sendsmstoemployee(string mobile, string message)
        {
            try
            {
                string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=ODMEGR&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
                + "&country=91&message=" + message;

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private string PopulateBody(string applicant, int id, string url, string month, string organisation)
        {
            string body = string.Empty;
            string encode = Encodings.Base64EncodingMethod(id.ToString());
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Salarytemplate.html")))
            {

                body = reader.ReadToEnd().Replace("{{applicant}}", applicant).Replace("{{month}}", month).Replace("{{href}}", url).Replace("{{organisation}}", organisation);

            }

            return body;
        }

        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }

        #endregion
    }
}