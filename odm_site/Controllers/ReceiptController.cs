﻿using ClosedXML.Excel;
using odm_site.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace odm_site.Controllers
{
    public class ReceiptController : Controller
    {
        DB_ODMSchoolEntities dbContext = new DB_ODMSchoolEntities();
        //
        // GET: /Receipt/
        [HttpGet]
        public ActionResult Index()
        
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string Username, string Password)
        {
            if (Username == "Admin")
            {
                if (Password == "admin@1234")
                {
                    Session["user_id"] = 121;
                    return RedirectToAction("Dashboard");
                }
                else
                {
                    ViewBag.errormessage = "Incorrect Password... Please try again";
                }
            }
            else
            {
                ViewBag.errormessage = "Invalid Username and Password... Please try again";
            }
            return View();
        }
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Receipt");
        }
        public ActionResult Dashboard()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            return View();
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public class Student_Details_cls
        {
            public int Student_Id { get; set; }
            [Required(ErrorMessage = "Enter Student Name")]
            [Display(Name = "Student Name")]
            public string Student_Name { get; set; }
            [Required(ErrorMessage = "Enter School No")]
            [Display(Name = "School No")]
            public string School_No { get; set; }
            [Required(ErrorMessage = "Enter School Name")]
            [Display(Name = "School Name")]
            public string School_Name { get; set; }
            [Required(ErrorMessage = "Enter Class Name")]
            [Display(Name = "Class Name")]
            public string Class_Name { get; set; }
            [Required(ErrorMessage = "Enter Section Name")]
            [Display(Name = "Section Name")]
            public string Section_Name { get; set; }
            [Required(ErrorMessage = "Enter Phone Number 1")]
            [Display(Name = "Phone Number 1")]
            public string Phone_No_1 { get; set; }
            [Display(Name = "Phone Number 2")]
            public string Phone_No_2 { get; set; }
            [Required(ErrorMessage = "Enter Email Id")]
            [Display(Name = "Email Id")]
            public string Email_ID { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<System.DateTime> Inserted_Date { get; set; }
            public Nullable<System.DateTime> Modified_Date { get; set; }
        }
        public ActionResult StudentList()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            var s = dbContext.Tbl_Student_Details.Where(a => a.Is_Active == true).ToList();
            return View(s);
        }
        public ActionResult CreateOrEditStudent(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            Student_Details_cls ob = new Student_Details_cls();
            if (id == null)
            {
                ob.Student_Id = 0;
            }
            else
            {
                var s = dbContext.Tbl_Student_Details.Where(a => a.Student_Id == id).FirstOrDefault();
                ob.Class_Name = s.Class_Name;
                ob.Email_ID = s.Email_ID;
                ob.Phone_No_1 = s.Phone_No_1;
                ob.Phone_No_2 = s.Phone_No_2;
                ob.School_Name = s.School_Name;
                ob.School_No = s.School_No;
                ob.Section_Name = s.Section_Name;
                ob.Student_Id = s.Student_Id;
                ob.Student_Name = s.Student_Name;

            }
            return View(ob);
        }
        [HttpPost]
        public ActionResult SaveOrUpdateStudent(Student_Details_cls s)
        {
            if (ModelState.IsValid)
            {
                if (s.Student_Id == 0)
                {
                    Tbl_Student_Details ob = new Tbl_Student_Details();
                    ob.Class_Name = s.Class_Name;
                    ob.Email_ID = s.Email_ID;
                    ob.Phone_No_1 = s.Phone_No_1;
                    ob.Phone_No_2 = s.Phone_No_2;
                    ob.School_Name = s.School_Name;
                    ob.School_No = s.School_No;
                    ob.Section_Name = s.Section_Name;
                    ob.Student_Id = s.Student_Id;
                    ob.Student_Name = s.Student_Name;
                    ob.Is_Active = true;
                    ob.Inserted_Date = DateTime.Now;
                    ob.Modified_Date = DateTime.Now;
                    dbContext.Tbl_Student_Details.Add(ob);
                    dbContext.SaveChanges();
                }
                else
                {
                    Tbl_Student_Details ob = dbContext.Tbl_Student_Details.Where(a => a.Student_Id == s.Student_Id).FirstOrDefault();
                    ob.Class_Name = s.Class_Name;
                    ob.Email_ID = s.Email_ID;
                    ob.Phone_No_1 = s.Phone_No_1;
                    ob.Phone_No_2 = s.Phone_No_2;
                    ob.School_Name = s.School_Name;
                    ob.School_No = s.School_No;
                    ob.Section_Name = s.Section_Name;
                    // ob.Student_Id = s.Student_Id;
                    ob.Student_Name = s.Student_Name;
                    ob.Is_Active = true;
                    ob.Modified_Date = DateTime.Now;
                    dbContext.SaveChanges();
                }
                TempData["SuccessMessage"] = "Saved Successfully";
                return RedirectToAction("StudentList");
            }
            return View("CreateOrEditStudent", s);
        }
        public ActionResult RemoveStudentById(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            Tbl_Student_Details ob = dbContext.Tbl_Student_Details.Where(a => a.Student_Id == id).FirstOrDefault();

            ob.Is_Active = false;
            ob.Modified_Date = DateTime.Now;
            dbContext.SaveChanges();

            TempData["SuccessMessage"] = "Removed Successfully";
            return RedirectToAction("StudentList");
        }
        public ActionResult DownloadSampleStudentExcel()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Student_Name", typeof(string));
            validationTable.Columns.Add("School_No", typeof(string));
            validationTable.Columns.Add("School_Name", typeof(string));
            validationTable.Columns.Add("Class_Name", typeof(string));
            validationTable.Columns.Add("Section_Name", typeof(string));
            validationTable.Columns.Add("Phone_No_1", typeof(decimal));
            validationTable.Columns.Add("Phone_No_2", typeof(decimal));
            validationTable.Columns.Add("Email_ID", typeof(decimal));
            validationTable.TableName = "Student_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", "SampleStudent_Details.xlsx");
        }
        [HttpPost]
        public ActionResult UploadStudentinfo(HttpPostedFileBase file)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("StudentList");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Student_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr["Student_Name"].ToString() != "")
                                {
                                    Tbl_Student_Details inv = new Tbl_Student_Details();
                                    //var s = dbContext.Tbl_Student_Details.Where(a => a.Is_Active == true).LastOrDefault();
                                    //if (s == null)
                                    //{
                                    //    inv.Invoice_Code = 100001;
                                    //}
                                    //else
                                    //{
                                    //    inv.Invoice_Code = 100000 + s.ID + 1;
                                    //}

                                    inv.Class_Name = dr["Class_Name"].ToString();
                                    inv.Email_ID = dr["Email_ID"].ToString();
                                    inv.Phone_No_1 = dr["Phone_No_1"].ToString();
                                    inv.Phone_No_2 = dr["Phone_No_2"].ToString();
                                    inv.School_Name = dr["School_Name"].ToString();
                                    inv.School_No = dr["School_No"].ToString(); ;
                                    inv.Section_Name = dr["Section_Name"].ToString(); ;
                                    inv.Is_Active = true;
                                    inv.Inserted_Date = DateTime.Now;
                                    inv.Modified_Date = DateTime.Now;
                                    inv.Student_Name = dr["Student_Name"].ToString();
                                    dbContext.Tbl_Student_Details.Add(inv);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }
                    TempData["SuccessMessage"] = "Student Details Entered Successfully";
                    return RedirectToAction("StudentList");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("StudentList");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("StudentList");
            }
        }


        #region Purposes
        public class purpose_cls
        {

            public int Purpose_Id { get; set; }
            [Display(Name = "Purpose")]
            [Required(ErrorMessage = "Enter Purpose")]
            public string Purpose_Name { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<System.DateTime> Inserted_On { get; set; }
        }
        public ActionResult PurposeList()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            var s = dbContext.Tbl_Purpose.Where(a => a.Is_Active == true).ToList();
            return View(s);
        }
        public ActionResult CreateOrEditPurpose(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            purpose_cls ob = new purpose_cls();
            if (id == null)
            {
                ob.Purpose_Id = 0;
            }
            else
            {
                var s = dbContext.Tbl_Purpose.Where(a => a.Purpose_Id == id).FirstOrDefault();
                ob.Purpose_Name = s.Purpose_Name;
                ob.Purpose_Id = s.Purpose_Id;
            }
            return View(ob);
        }
        [HttpPost]
        public ActionResult SaveOrUpdatePurpose(purpose_cls s)
        {
            if (ModelState.IsValid)
            {
                if (s.Purpose_Id == 0)
                {
                    Tbl_Purpose ob = new Tbl_Purpose();
                    ob.Purpose_Name = s.Purpose_Name;
                    ob.Is_Active = true;
                    ob.Inserted_On = DateTime.Now;
                    dbContext.Tbl_Purpose.Add(ob);
                    dbContext.SaveChanges();
                }
                else
                {
                    Tbl_Purpose ob = dbContext.Tbl_Purpose.Where(a => a.Purpose_Id == s.Purpose_Id).FirstOrDefault();
                    ob.Purpose_Name = s.Purpose_Name;
                    ob.Is_Active = true;
                    dbContext.SaveChanges();
                }
                TempData["SuccessMessage"] = "Saved Successfully";
                return RedirectToAction("PurposeList");
            }
            return View("CreateOrEditPurpose", s);
        }
        public ActionResult RemovePurposeById(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            Tbl_Purpose ob = dbContext.Tbl_Purpose.Where(a => a.Purpose_Id == id).FirstOrDefault();
            ob.Is_Active = false;
            dbContext.SaveChanges();

            TempData["SuccessMessage"] = "Removed Successfully";
            return RedirectToAction("PurposeList");
        }
        #endregion

        #region Paymentmode
        public class Paymentmode_cls
        {

            public int Paymentmode_Id { get; set; }
            [Display(Name = "Payment mode")]
            [Required(ErrorMessage = "Enter Payment mode")]
            public string Paymentmode_Name { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<System.DateTime> Inserted_On { get; set; }
        }
        public ActionResult PaymentmodeList()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            var s = dbContext.Tbl_Paymentmode.Where(a => a.Is_Active == true).ToList();
            return View(s);
        }
        public ActionResult CreateOrEditPaymentmode(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            Paymentmode_cls ob = new Paymentmode_cls();
            if (id == null)
            {
                ob.Paymentmode_Id = 0;
            }
            else
            {
                var s = dbContext.Tbl_Paymentmode.Where(a => a.Paymentmode_Id == id).FirstOrDefault();
                ob.Paymentmode_Name = s.Paymentmode_Name;
                ob.Paymentmode_Id = s.Paymentmode_Id;
            }
            return View(ob);
        }
        [HttpPost]
        public ActionResult SaveOrUpdatePaymentmode(Paymentmode_cls s)
        {
            if (ModelState.IsValid)
            {
                if (s.Paymentmode_Id == 0)
                {
                    Tbl_Paymentmode ob = new Tbl_Paymentmode();
                    ob.Paymentmode_Name = s.Paymentmode_Name;
                    ob.Is_Active = true;
                    ob.Inserted_On = DateTime.Now;
                    dbContext.Tbl_Paymentmode.Add(ob);
                    dbContext.SaveChanges();
                }
                else
                {
                    Tbl_Paymentmode ob = dbContext.Tbl_Paymentmode.Where(a => a.Paymentmode_Id == s.Paymentmode_Id).FirstOrDefault();
                    ob.Paymentmode_Name = s.Paymentmode_Name;
                    ob.Is_Active = true;
                    dbContext.SaveChanges();
                }
                TempData["SuccessMessage"] = "Saved Successfully";
                return RedirectToAction("PaymentmodeList");
            }
            return View("CreateOrEditPaymentmode", s);
        }
        public ActionResult RemovePaymentmodeById(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            Tbl_Paymentmode ob = dbContext.Tbl_Paymentmode.Where(a => a.Paymentmode_Id == id).FirstOrDefault();
            ob.Is_Active = false;
            dbContext.SaveChanges();

            TempData["SuccessMessage"] = "Removed Successfully";
            return RedirectToAction("PaymentmodeList");
        }
        #endregion



        #region Receipt Generator
        public class Receipt_cls
        {
            public int Receipt_Id { get; set; }
            public Nullable<int> Student_Id { get; set; }
            public string Student_Name { get; set; }
            public string School_No { get; set; }
            public string School_Name { get; set; }
            public string Class_Name { get; set; }
            public string Section_Name { get; set; }
            public string Phone_No_1 { get; set; }
            public string Phone_No_2 { get; set; }
            public string Email_ID { get; set; }
            public Nullable<int> Purpose1 { get; set; }
            public string Purpose_Name1 { get; set; }
            public string Amount1 { get; set; }
            public Nullable<int> Purpose2 { get; set; }
            public string Purpose_Name2 { get; set; }
            public string Amount2 { get; set; }
            public Nullable<int> Purpose3 { get; set; }
            public string Purpose_Name3 { get; set; }
            public string Amount3 { get; set; }
            public Nullable<int> Purpose4 { get; set; }
            public string Purpose_Name4 { get; set; }
            public string Amount4 { get; set; }
            public Nullable<decimal> Adjustment_Amount { get; set; }
            public string Note { get; set; }
            public Nullable<decimal> Grand_Total { get; set; }
            public Nullable<decimal> Total_Amount { get; set; }
            public string Invoice_No { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<System.DateTime> inserted_on { get; set; }
            public Nullable<System.DateTime> modified_on { get; set; }
            public Nullable<int> Paymentmode_Id { get; set; }
            public Nullable<System.DateTime> Payment_Date { get; set; }
            public string Reference_No { get; set; }
            public string Paymentmode { get; set; }
           
        }
        public class studentddlcls
        {
            public int id { get; set; }
            public string name { get; set; }
        }
        public ActionResult ReceiptGeneratedList(int? student, string cls, string sec, string school_name,DateTime? paymentdate,int? paymentmode)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            ViewBag.student = student;
            ViewBag.cls = cls;
            ViewBag.sec = sec;
            ViewBag.school_name = school_name;
            //ViewBag.school_no = school_no;
            ViewBag.paymentmode = paymentmode;
            var receipts = dbContext.Tbl_Receipt_Details.Where(a => a.Is_Active == true).ToList();
         
            var studentdetails = dbContext.Tbl_Student_Details.ToList();
            var purposes = dbContext.Tbl_Purpose.ToList();
            var paymentmodelist=dbContext.Tbl_Paymentmode.Where(a=>a.Is_Active==true).Distinct().ToList();
            var res = (from a in receipts
                       join b in studentdetails on a.Student_Id equals b.Student_Id
                       select new Receipt_cls{ 
                       Adjustment_Amount=a.Adjustment_Amount,
                       Amount1=(a.Amount1==null && a.Amount1==0)?"":a.Amount1.ToString(),
                       Amount2 = (a.Amount2 == null && a.Amount2 == 0) ? "" : a.Amount2.ToString(),
                       Amount3 = (a.Amount3 == null && a.Amount3 == 0) ? "" : a.Amount3.ToString(),
                       Amount4 = (a.Amount4 == null && a.Amount4 == 0) ? "" : a.Amount4.ToString(),
                       Class_Name=b.Class_Name,
                       Email_ID=b.Email_ID,
                       Grand_Total=a.Grand_Total,
                       inserted_on=a.inserted_on,
                       Invoice_No=a.Invoice_No,
                       Is_Active=a.Is_Active,
                       modified_on=a.modified_on,
                       Note=a.Note,
                       Phone_No_1=b.Phone_No_1,
                       Phone_No_2=b.Phone_No_2,
                       Purpose_Name1=(a.Purpose1!=null && a.Purpose1!=0)?purposes.Where(m=>m.Purpose_Id==a.Purpose1).FirstOrDefault().Purpose_Name:"",
                       Purpose_Name2 = (a.Purpose2 != null && a.Purpose2 != 0) ? purposes.Where(m => m.Purpose_Id == a.Purpose2).FirstOrDefault().Purpose_Name : "",
                       Purpose_Name3 = (a.Purpose3 != null && a.Purpose3 != 0) ? purposes.Where(m => m.Purpose_Id == a.Purpose3).FirstOrDefault().Purpose_Name : "",
                       Purpose_Name4 = (a.Purpose4 != null && a.Purpose4 != 0) ? purposes.Where(m => m.Purpose_Id == a.Purpose4).FirstOrDefault().Purpose_Name : "",
                       Purpose1=a.Purpose1,
                       Purpose2=a.Purpose2,
                       Purpose3=a.Purpose3,
                       Purpose4=a.Purpose4,
                       Receipt_Id=a.Receipt_Id,
                       School_Name=b.School_Name,
                       School_No=b.School_No,
                       Section_Name=b.Section_Name,
                       Student_Id=a.Student_Id,
                       Student_Name=b.Student_Name,
                       Total_Amount=a.Total_Amount,
                       Payment_Date=a.Payment_Date,
                       Paymentmode_Id=a.Paymentmode_Id,
                       Reference_No=a.Reference_No,
                       Paymentmode = paymentmodelist.Where(h=>h.Paymentmode_Id==a.Paymentmode_Id).FirstOrDefault().Paymentmode_Name
                       }).ToList();
            ViewBag.paymentmodelist = paymentmodelist;
            ViewBag.studentnamelist = studentdetails == null ? null : studentdetails.Select(a => new studentddlcls { name = (a.Student_Name + "-" + a.School_No + "-" + a.Class_Name + "-" + a.Section_Name), id = a.Student_Id }).Distinct().ToList();
            ViewBag.classlist = studentdetails == null ? null : studentdetails.Select(a => a.Class_Name).Distinct().ToList();
            ViewBag.sectionlist = studentdetails == null ? null : studentdetails.Select(a => a.Section_Name).Distinct().ToList();
            ViewBag.schoolnamelist = studentdetails == null ? null : studentdetails.Select(a => a.School_Name).Distinct().ToList();
          //  ViewBag.schoolnolist = res == null ? null : res.Select(a => a.School_No).Distinct().ToList();
            if (student != null && student != 0)
            {
                res = res.Where(a => a.Student_Id == student).ToList();
            }
            if (cls != null && cls != "")
            {
                res = res.Where(a => a.Class_Name == cls).ToList();
            }
            if (sec != null && sec != "")
            {
                res = res.Where(a => a.Section_Name == sec).ToList();
            }
            if (school_name != null && school_name != "")
            {
                res = res.Where(a => a.School_Name == school_name).ToList();
            }
            //if (school_no != null && school_no != "")
            //{
            //    res = res.Where(a => a.School_No == school_no).ToList();
            //}
            if (paymentdate != null)
            {
                res = res.Where(a => a.Payment_Date == paymentdate).ToList();
            }
            if (paymentmode != null && paymentmode!=0)
            {
                res = res.Where(a => a.Paymentmode_Id == paymentmode).ToList();
            }
            Session["receipt_result"] = res;
            ViewBag.receiptdetails = res;
            return View();
        }
        public ActionResult DownloadReceiptGenerateExcel()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            XLWorkbook oWB = new XLWorkbook();
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("School_No", typeof(string));
            validationTable.Columns.Add("Student_Name", typeof(string));
            validationTable.Columns.Add("Class_Name", typeof(string));
            validationTable.Columns.Add("Section_Name", typeof(string));
            validationTable.Columns.Add("School_Name", typeof(string));
            validationTable.Columns.Add("Purpose1", typeof(string));
            validationTable.Columns.Add("Amount1", typeof(string));
            validationTable.Columns.Add("Purpose2", typeof(string));
            validationTable.Columns.Add("Amount2", typeof(string));
            validationTable.Columns.Add("Purpose3", typeof(string));
            validationTable.Columns.Add("Amount3", typeof(string));
            validationTable.Columns.Add("Purpose4", typeof(string));
            validationTable.Columns.Add("Amount4", typeof(string));
            validationTable.Columns.Add("Adjustment_Amount", typeof(string));
            validationTable.Columns.Add("Note", typeof(string));
            validationTable.Columns.Add("Grand_Total", typeof(string));
            validationTable.Columns.Add("Total_Amount", typeof(string));
            validationTable.Columns.Add("Payment_Mode", typeof(string));
            validationTable.Columns.Add("Payment_Date(dd-MM-yyyy)");
            validationTable.Columns.Add("Reference_No");

            List<Receipt_cls> res = Session["receipt_result"] as List<Receipt_cls>;
            DataRow vdr;
            foreach(var m in res)
            {
                vdr = validationTable.NewRow();
                vdr["School_No"]=m.School_No;
                vdr["Student_Name"]=m.Student_Name;
                vdr["Class_Name"]=m.Class_Name;
                vdr["Section_Name"]=m.Section_Name;
                vdr["School_Name"]=m.School_Name;
                vdr["Purpose1"]=m.Purpose_Name1;
                vdr["Amount1"]=m.Amount1;
                vdr["Purpose2"]=m.Purpose_Name2;
                vdr["Amount2"]=m.Amount2;
                vdr["Purpose3"]=m.Purpose_Name3;
                vdr["Amount3"]=m.Amount3;
                vdr["Purpose4"]=m.Purpose_Name4;
                vdr["Amount4"]=m.Amount4;
                vdr["Adjustment_Amount"]=m.Adjustment_Amount;
                vdr["Note"]=m.Note;
                vdr["Grand_Total"]=m.Grand_Total;
                vdr["Total_Amount"]=m.Total_Amount;
                vdr["Payment_Mode"]=m.Paymentmode;
                vdr["Payment_Date(dd-MM-yyyy)"]=m.Payment_Date.Value.ToString("dd-MM-yyyy");
                vdr["Reference_No"]=m.Reference_No;
                validationTable.Rows.Add(vdr);
            }
            validationTable.TableName = "Receipt_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
           
           

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", "Receipt_List.xlsx");
        }
        public ActionResult DownloadSampleReceiptGenerateExcel()
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            XLWorkbook oWB = new XLWorkbook();
            DataTable gdt = new DataTable("Payment_Mode");
            gdt.Columns.Add("Name");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var g = dbContext.Tbl_Paymentmode.Where(a => a.Is_Active == true).ToList();
            foreach (var gs in g.ToList())
            {
                DataRow dr = gdt.NewRow();
                dr["Name"] = gs.Paymentmode_Name;
                gdt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo1 = gdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet1 = oWB.Worksheet(1);

            DataTable pdt = new DataTable("Purpose");
            pdt.Columns.Add("Name");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var p = dbContext.Tbl_Purpose.Where(a => a.Is_Active == true).ToList();
            foreach (var ps in p.ToList())
            {
                DataRow dr = pdt.NewRow();
                dr["Name"] = ps.Purpose_Name;
                pdt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo2 = pdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(pdt);
            var worksheet2 = oWB.Worksheet(2);



            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("School_No", typeof(string));
            validationTable.Columns.Add("Student_Name", typeof(string));
            validationTable.Columns.Add("Purpose1", typeof(string));
            validationTable.Columns.Add("Amount1", typeof(string));
            validationTable.Columns.Add("Purpose2", typeof(string));
            validationTable.Columns.Add("Amount2", typeof(string));
            validationTable.Columns.Add("Purpose3", typeof(string));
            validationTable.Columns.Add("Amount3", typeof(string));
            validationTable.Columns.Add("Purpose4", typeof(string));
            validationTable.Columns.Add("Amount4", typeof(string));
            validationTable.Columns.Add("Adjustment_Amount", typeof(string));
            validationTable.Columns.Add("Note", typeof(string));
            validationTable.Columns.Add("Grand_Total", typeof(string));
            validationTable.Columns.Add("Total_Amount", typeof(string));
            validationTable.Columns.Add("Payment_Mode", typeof(string));
            validationTable.Columns.Add("Payment_Date(dd-MM-yyyy)");
            validationTable.Columns.Add("Reference_No");
            validationTable.TableName = "Receipt_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(15).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(3).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(5).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(7).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(9).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Cell(2,4).FormulaA1 = "0";
            worksheet.Cell(2, 6).FormulaA1 = "0";
            worksheet.Cell(2, 8).FormulaA1 = "0";
            worksheet.Cell(2, 10).FormulaA1 = "0";
            worksheet.Cell(2, 13).FormulaA1 = "=SUM(D2,F2,H2,J2)";
            worksheet.Cell(2, 14).FormulaA1 = "=(M2-K2)";
          //  worksheet.Cell(2, 16).DataType = XLDataType.DateTime;
           // worksheet.Column(16).SetDataValidation().Date.EqualOrLessThan(DateTime.Now.Date);
            worksheet.Column(16).SetDataValidation().Date.EqualOrGreaterThan(Convert.ToDateTime("1990-01-31"));
            worksheet1.Hide();
            worksheet2.Hide();
            
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", "SampleReceipt_Details.xlsx");
        }
        [HttpPost]
        public ActionResult UploadReceiptGenerateinfo(HttpPostedFileBase file)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            try
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("ReceiptGeneratedList");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Receipt_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            var studentlist = dbContext.Tbl_Student_Details.Where(a => a.Is_Active == true).ToList();
                            var purposelist = dbContext.Tbl_Purpose.Where(a => a.Is_Active == true).ToList();
                            var paymentmodelist = dbContext.Tbl_Paymentmode.Where(a => a.Is_Active == true).ToList();
                            foreach (DataRow dr in dt.Rows)
                            {
                                Tbl_Receipt_Details inv = new Tbl_Receipt_Details();
                                var s = dbContext.Tbl_Receipt_Details.ToList().LastOrDefault();
                                if (s == null)
                                {
                                    inv.Invoice_No = "ODM/" + 100001;
                                }
                                else
                                {
                                    inv.Invoice_No = "ODM/" + (100000 + s.Receipt_Id + 1);
                                }
                                var student = studentlist.Where(a => a.School_No == dr["School_No"].ToString()).FirstOrDefault();
                                inv.Student_Id = student.Student_Id;
                                inv.Adjustment_Amount = Convert.ToDecimal(dr["Adjustment_Amount"].ToString());
                                inv.Amount1 = (dr["Purpose1"].ToString() == "") ? null : Convert.ToDecimal(dr["Amount1"].ToString()) as Nullable<decimal>;
                                inv.Amount2 = (dr["Purpose2"].ToString() == "") ? null : Convert.ToDecimal(dr["Amount2"].ToString()) as Nullable<decimal>;
                                inv.Amount3 = (dr["Purpose3"].ToString() == "") ? null : Convert.ToDecimal(dr["Amount3"].ToString()) as Nullable<decimal>;
                                inv.Amount4 = (dr["Purpose4"].ToString() == "") ? null : Convert.ToDecimal(dr["Amount4"].ToString()) as Nullable<decimal>;
                                inv.Purpose1 = (dr["Purpose1"].ToString() == "") ? null : purposelist.Where(a => a.Purpose_Name == dr["Purpose1"].ToString()).FirstOrDefault().Purpose_Id as Nullable<int>;
                                inv.Purpose2 = (dr["Purpose2"].ToString() == "") ? null : purposelist.Where(a => a.Purpose_Name == dr["Purpose2"].ToString()).FirstOrDefault().Purpose_Id as Nullable<int>;
                                inv.Purpose3 = (dr["Purpose3"].ToString() == "") ? null : purposelist.Where(a => a.Purpose_Name == dr["Purpose3"].ToString()).FirstOrDefault().Purpose_Id as Nullable<int>;
                                inv.Purpose4 = (dr["Purpose4"].ToString() == "") ? null : purposelist.Where(a => a.Purpose_Name == dr["Purpose4"].ToString()).FirstOrDefault().Purpose_Id as Nullable<int>;
                                inv.Note = dr["Note"].ToString();
                                inv.Total_Amount = Convert.ToDecimal(dr["Total_Amount"].ToString());
                                inv.Grand_Total = Convert.ToDecimal(dr["Grand_Total"].ToString());
                                inv.Paymentmode_Id = (dr["Payment_Mode"].ToString() == "") ? null : paymentmodelist.Where(a => a.Paymentmode_Name == dr["Payment_Mode"].ToString()).FirstOrDefault().Paymentmode_Id as Nullable<int>;
                                inv.Payment_Date = (dr["Payment_Date(dd-MM-yyyy)"].ToString() == "") ? null : Convert.ToDateTime(dr["Payment_Date(dd-MM-yyyy)"].ToString()) as Nullable<DateTime>;
                                inv.Reference_No = dr["Reference_No"].ToString();
                                inv.Is_Active = true;
                                inv.inserted_on = DateTime.Now;
                                inv.modified_on = DateTime.Now;
                                dbContext.Tbl_Receipt_Details.Add(inv);
                                dbContext.SaveChanges();
                                var empid = inv.Receipt_Id;
                                string encode = Encodings.Base64EncodingMethod(empid.ToString());
                                string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=receipt&sid=" + encode;
                                //bitly b = new bitly();
                                string shortlink = ToTinyURLS(url);
                                if (student.Phone_No_1 != "")
                                {
                                    Sendsmstoemployee(student.Phone_No_1, "Dear Parent, \nPlease find the money receipt of your child " + student.Student_Name + " of " + student.School_Name + " and " + student.Class_Name + ". Click on the link and save the money receipt.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                                }

                                if (student.Email_ID != "")
                                {
                                    string body = PopulateBody(student.School_Name, student.School_Name, student.Class_Name, shortlink);
                                    SendHtmlFormattedEmail(student.Email_ID, "Receipt | ODM Public School", body);
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }
                    TempData["SuccessMessage"] = "Student Details Entered Successfully";
                    return RedirectToAction("ReceiptGeneratedList");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("ReceiptGeneratedList");
                }
            }
            catch(Exception e2)
            {
                TempData["ErrorMessage"] =e2.InnerException;
                return RedirectToAction("ReceiptGeneratedList");
            }
        }

        public ActionResult SendReceiptByID(int id,int stdid)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            Tbl_Receipt_Details ob = dbContext.Tbl_Receipt_Details.Where(a => a.Receipt_Id == id).FirstOrDefault();
            Tbl_Student_Details student = dbContext.Tbl_Student_Details.Where(a => a.Student_Id == ob.Student_Id).FirstOrDefault();
            string encode = Encodings.Base64EncodingMethod(id.ToString());
            string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=receipt&sid=" + encode;
            //bitly b = new bitly();
            string shortlink = ToTinyURLS(url);
            if (student.Phone_No_1 != "")
            {
                Sendsmstoemployee(student.Phone_No_1, "Dear Parent, \nPlease find the money receipt of your child " + student.Student_Name + " of " + student.School_Name + " and " + student.Class_Name + ". Click on the link and save the money receipt.- " + shortlink + " \n\n Regards\n ODM Educational Group");
            }

            if (student.Email_ID != "")
            {
                string body = PopulateBody(student.School_Name, student.School_Name, student.Class_Name, shortlink);
                SendHtmlFormattedEmail(student.Email_ID, "Receipt | ODM Public School", body);
            }
            return RedirectToAction("ReceiptGeneratedList");
        }
        public class Receiptentry_Cls
        {
            public int Receipt_Id { get; set; }
            [Required(ErrorMessage = "Select Student")]
            [Display(Name = "Student")]
            public Nullable<int> Student_Id { get; set; }
            [Required(ErrorMessage = "Select Purpose 1")]
            [Display(Name = "Purpose 1")]
            public Nullable<int> Purpose1 { get; set; }
            [Required(ErrorMessage = "Enter Amount 1")]
            [Display(Name = "Amount 1")]
            public Nullable<decimal> Amount1 { get; set; }
            [Display(Name = "Purpose 2")]
            public Nullable<int> Purpose2 { get; set; }
            [Display(Name = "Amount 2")]
            public Nullable<decimal> Amount2 { get; set; }
            [Display(Name = "Purpose 3")]
            public Nullable<int> Purpose3 { get; set; }
            [Display(Name = "Amount 3")]
            public Nullable<decimal> Amount3 { get; set; }
            [Display(Name = "Purpose 4")]
            public Nullable<int> Purpose4 { get; set; }
            [Display(Name = "Amount 4")]
            public Nullable<decimal> Amount4 { get; set; }
            [Required(ErrorMessage = "Enter Adjustment Amount")]
            [Display(Name = "Adjustment Amount")]
            public Nullable<decimal> Adjustment_Amount { get; set; }
            [Display(Name = "Note")]
            public string Note { get; set; }
            [Required(ErrorMessage = "Enter Grand Total")]
            [Display(Name = "Grand Total")]
            public Nullable<decimal> Grand_Total { get; set; }
            [Required(ErrorMessage = "Enter Total Amount")]
            [Display(Name = "Total Amount")]
            public Nullable<decimal> Total_Amount { get; set; }
            [Required(ErrorMessage = "Enter Payment Date")]
            [Display(Name = "Payment Date")]
            public Nullable<DateTime> Payment_Date { get; set; }
            [Required(ErrorMessage = "Enter Payment Mode")]
            [Display(Name = "Payment Mode")]
            public Nullable<int> Payment_Mode { get; set; }
            [Required(ErrorMessage = "Enter Reference No")]
            [Display(Name = "Reference No")]
            public string Reference_No { get; set; }
            public string Invoice_No { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<System.DateTime> inserted_on { get; set; }
            public Nullable<System.DateTime> modified_on { get; set; }
        }

        public ActionResult CreateOrEditReceipt(int? id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            var stdlist = dbContext.Tbl_Student_Details.Where(a => a.Is_Active == true).ToList();
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (Tbl_Student_Details row in stdlist)
            {
                list.Add(new SelectListItem()
                {
                    Text = row.Student_Name + "-" + row.School_No+"-"+row.Class_Name+"-"+row.Section_Name,
                    Value = row.Student_Id.ToString()
                });
            }
            ViewBag.studentlist = list;
            var purposelist = dbContext.Tbl_Purpose.Where(a => a.Is_Active == true).ToList();

            list = new List<SelectListItem>();
            foreach (var row in purposelist)
            {
                list.Add(new SelectListItem()
                {
                    Text = row.Purpose_Name,
                    Value = row.Purpose_Id.ToString()
                });
            }
            ViewBag.purposelist = list;

            var paymentmodelist = dbContext.Tbl_Paymentmode.Where(a => a.Is_Active == true).ToList();

            list = new List<SelectListItem>();
            foreach (var row in paymentmodelist)
            {
                list.Add(new SelectListItem()
                {
                    Text = row.Paymentmode_Name,
                    Value = row.Paymentmode_Id.ToString()
                });
            }
            ViewBag.paymentmodelist = list;

            Receiptentry_Cls ob = new Receiptentry_Cls();
            if (id == null)
            {
                ob.Receipt_Id = 0;
            }
            else
            {
                var rec = dbContext.Tbl_Receipt_Details.Where(a => a.Receipt_Id == id).FirstOrDefault();
                ob.Adjustment_Amount = rec.Adjustment_Amount;
                ob.Amount1 = rec.Amount1;
                ob.Amount2 = rec.Amount2;
                ob.Amount3 = rec.Amount3;
                ob.Amount4 = rec.Amount4;
                ob.Grand_Total = rec.Grand_Total;
                ob.Invoice_No = rec.Invoice_No;
                ob.Note = rec.Note;
                ob.Purpose1 = rec.Purpose1;
                ob.Purpose2 = rec.Purpose2;
                ob.Purpose3 = rec.Purpose3;
                ob.Purpose4 = rec.Purpose4;
                ob.Receipt_Id = rec.Receipt_Id;
                ob.Student_Id = rec.Student_Id;
                ob.Total_Amount = rec.Total_Amount;
                ob.Payment_Date = rec.Payment_Date;
                ob.Payment_Mode = rec.Paymentmode_Id;
                ob.Reference_No = rec.Reference_No;

            }
            return View(ob);
        }
        [HttpPost]
        public ActionResult SaveOrUpdateReceipt(Receiptentry_Cls rec)
        {
            if (ModelState.IsValid)
            {
                if (rec.Receipt_Id == 0)
                {
                    Tbl_Receipt_Details ob = new Tbl_Receipt_Details();
                    ob.Adjustment_Amount = rec.Adjustment_Amount;
                    var s = dbContext.Tbl_Receipt_Details.ToList().LastOrDefault();
                    if (s == null)
                    {
                        ob.Invoice_No = "ODM/" + 100001;
                    }
                    else
                    {
                        ob.Invoice_No = "ODM/" + (100000 + s.Receipt_Id + 1);
                    }
                    ob.Amount1 = rec.Amount1;
                    ob.Amount2 = rec.Amount2;
                    ob.Amount3 = rec.Amount3;
                    ob.Amount4 = rec.Amount4;
                    ob.Grand_Total = rec.Grand_Total;
                    ob.Note = rec.Note;
                    ob.Purpose1 = rec.Purpose1;
                    ob.Purpose2 = rec.Purpose2;
                    ob.Purpose3 = rec.Purpose3;
                    ob.Purpose4 = rec.Purpose4;
                    ob.Receipt_Id = rec.Receipt_Id;
                    ob.Student_Id = rec.Student_Id;
                    ob.Total_Amount = rec.Total_Amount;
                    ob.Payment_Date = rec.Payment_Date;
                    ob.Paymentmode_Id = rec.Payment_Mode;
                    ob.Reference_No = rec.Reference_No;
                    ob.Is_Active = true;
                    ob.inserted_on = DateTime.Now;
                    ob.modified_on = DateTime.Now;
                    dbContext.Tbl_Receipt_Details.Add(ob);
                    dbContext.SaveChanges();
                    Tbl_Student_Details student = dbContext.Tbl_Student_Details.Where(a => a.Student_Id == ob.Student_Id).FirstOrDefault();
                    string encode = Encodings.Base64EncodingMethod(ob.Receipt_Id.ToString());
                    string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=receipt&sid=" + encode;
                    //bitly b = new bitly();
                    string shortlink = ToTinyURLS(url);
                    if (student.Phone_No_1 != "")
                    {
                        Sendsmstoemployee(student.Phone_No_1, "Dear Parent, \nPlease find the money receipt of your child " + student.Student_Name + " of " + student.School_Name + " and " + student.Class_Name + ". Click on the link and save the money receipt.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                    }

                    if (student.Email_ID != "")
                    {
                        string body = PopulateBody(student.School_Name, student.School_Name, student.Class_Name, shortlink);
                        SendHtmlFormattedEmail(student.Email_ID, "Receipt | ODM Public School", body);
                    }
                }
                else
                {
                    Tbl_Receipt_Details ob = dbContext.Tbl_Receipt_Details.Where(a => a.Receipt_Id == rec.Receipt_Id).FirstOrDefault();
                    ob.Adjustment_Amount = rec.Adjustment_Amount;
                    ob.Amount1 = rec.Amount1;
                    ob.Amount2 = rec.Amount2;
                    ob.Amount3 = rec.Amount3;
                    ob.Amount4 = rec.Amount4;
                    ob.Grand_Total = rec.Grand_Total;
                    ob.Note = rec.Note;
                    ob.Purpose1 = rec.Purpose1;
                    ob.Purpose2 = rec.Purpose2;
                    ob.Purpose3 = rec.Purpose3;
                    ob.Purpose4 = rec.Purpose4;
                    ob.Receipt_Id = rec.Receipt_Id;
                    ob.Student_Id = rec.Student_Id;
                    ob.Total_Amount = rec.Total_Amount;
                    ob.Payment_Date = rec.Payment_Date;
                    ob.Paymentmode_Id = rec.Payment_Mode;
                    ob.Reference_No = rec.Reference_No;
                    ob.Is_Active = true;
                    ob.modified_on = DateTime.Now;
                    dbContext.SaveChanges();
                    Tbl_Student_Details student = dbContext.Tbl_Student_Details.Where(a => a.Student_Id == ob.Student_Id).FirstOrDefault();
                    string encode = Encodings.Base64EncodingMethod(ob.Receipt_Id.ToString());
                    string url = ConfigurationManager.AppSettings["domain"].ToString() + "Report/ReportPages/SalaryReport.aspx?type=receipt&sid=" + encode;
                    //bitly b = new bitly();
                    string shortlink = ToTinyURLS(url);
                    if (student.Phone_No_1 != "")
                    {
                        Sendsmstoemployee(student.Phone_No_1, "Dear Parent, \nPlease find the money receipt of your child " + student.Student_Name + " of " + student.School_Name + " and " + student.Class_Name + ". Click on the link and save the money receipt.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                    }

                    if (student.Email_ID != "")
                    {
                        string body = PopulateBody(student.School_Name, student.School_Name, student.Class_Name, shortlink);
                        SendHtmlFormattedEmail(student.Email_ID, "Receipt | ODM Public School", body);
                    }
                }
                TempData["SuccessMessage"] = "Saved Successfully";
                return RedirectToAction("ReceiptGeneratedList");
            }
            var stdlist = dbContext.Tbl_Student_Details.Where(a => a.Is_Active == true).ToList();
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (Tbl_Student_Details row in stdlist)
            {
                list.Add(new SelectListItem()
                {
                    Text = row.Student_Name + "-" + row.School_No + "-" + row.Class_Name + "-" + row.Section_Name,
                    Value = row.Student_Id.ToString()
                });
            }
            ViewBag.studentlist = list;
            var purposelist = dbContext.Tbl_Purpose.Where(a => a.Is_Active == true).ToList();

            list = new List<SelectListItem>();
            foreach (var row in purposelist)
            {
                list.Add(new SelectListItem()
                {
                    Text = row.Purpose_Name,
                    Value = row.Purpose_Id.ToString()
                });
            }
            ViewBag.purposelist = list;
            var paymentmodelist = dbContext.Tbl_Paymentmode.Where(a => a.Is_Active == true).ToList();

            list = new List<SelectListItem>();
            foreach (var row in paymentmodelist)
            {
                list.Add(new SelectListItem()
                {
                    Text = row.Paymentmode_Name,
                    Value = row.Paymentmode_Id.ToString()
                });
            }
            ViewBag.paymentmodelist = list;
            return View("CreateOrEditReceipt", rec);
        }
       public ActionResult RemoveReceipt(int id)
        {
            if (Session["user_id"] == null || Session["user_id"].ToString() != "121")
            {
                return RedirectToAction("LogOut", "Receipt");
            }
            var s = dbContext.Tbl_Receipt_Details.Where(a => a.Receipt_Id == id).FirstOrDefault();
            s.Is_Active = false;
            s.modified_on = DateTime.Now;
            dbContext.SaveChanges();
            TempData["SuccessMessage"] = "Removed Successfully";
            return RedirectToAction("ReceiptGeneratedList");
        }
        
        
        #endregion

        #region Mail and Messages

        public bool Sendsmstoemployee(string mobile, string message)
        {
            try
            {
                string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=ODMEGR&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
                + "&country=91&message=" + message;

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private string PopulateBody(string student,string school,string classname, string url)
        {
            string body = string.Empty;
            // string encode = Encodings.Base64EncodingMethod(id.ToString());
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/ReceiptTemplate.html")))
            {

                body = reader.ReadToEnd().Replace("{{Student_Name}}", student).Replace("{{Class}}", classname).Replace("{{School}}", school).Replace("{{href}}", url);

            }

            return body;
        }

        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("tracq@odmegroup.org");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "tracq@odmegroup.org";
                NetworkCred.Password = "odm@1234";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }

        #endregion
        #region Bitly

        protected string ToTinyURLS(string txt)
        {
            Regex regx = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                string tURL = MakeTinyUrl(match.Value);
                txt = txt.Replace(match.Value, tURL);
            }

            return txt;
        }

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 12)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return Url;
            }
        }

        #endregion
    }
}